import { React, AllWidgetProps, appActions, Immutable } from 'jimu-core'
import { IMConfig } from '../config'

import EyeIcon from './eye.png'
import './widget.scss'

const Widget = (props: AllWidgetProps<any>) => {

  const drawLineOfSight = () => {
    console.log(props.data.attributes); //geometry

    const sr = Immutable.asMutable(props.data.geometry.spatialReference);

    props.widgetProps.dispatch(
      appActions.widgetStatePropChange(
        'widget_226',
        "selectedLineOfSightGraphic",
        {
          attributes: props.data.attributes, 
          geometry: {
            x: props.data.geometry.x,
            y: props.data.geometry.y,
            z: props.data.geometry.z,
            spatialReference: sr
          }
        })
    );
  }

  return (
    <button className='zoom-renderer_btn' onClick={drawLineOfSight}><span>
      <img className='zoom-renderer_icon' src={EyeIcon} title="View Line Of Sight" />
      </span></button>
  )
}

export default Widget
