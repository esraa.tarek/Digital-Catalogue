import { useState, useEffect, useMemo } from 'react'

import { React, AllWidgetProps } from 'jimu-core'
import config from '../../config.json'

import FeatureLayer from 'esri/layers/FeatureLayer';
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis';

import cameraViewRenderer from '../../../cameraViewRenderer/src/runtime/widget'
import lineOfSightRenderer from '../../../lineOfSightRenderer/src/runtime/widget'

import * as AgGridReactLib from 'ag-grid-react'

import 'ag-grid-community/styles//ag-grid.css'
import 'ag-grid-community/styles//ag-theme-alpine.css'
import './widget.scss'

const Widget = (props: AllWidgetProps<any>) => {
  const {AgGridReact} = AgGridReactLib;

  const [gridData, setGridData] = useState<object[]>([])
  const [buildingUnitViewData, setBuildingUnitViewData] = useState([]);
  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()

  useEffect(() => {
    if (jimuMapView) {
      addPointsFL();
    }
  }, [jimuMapView])

  const addPointsFL = () =>{
    const layer = new FeatureLayer({
      url: config.dataFeatureLayerUrl
    });

    // Add the layer to the map (accessed through the Experience Builder JimuMapView data source)
    // jimuMapView.view.map.add(layer);

    layer.queryFeatures({ where: '1=1', returnGeometry: true, outFields: '*' })
    .then((data) => {
      console.log(data.features);
      setGridData(data.features);
    });

  }

  const columnDefs = [
    { headerName: 'Building Id', field: config.Building_id, width: 200, filter: 'agNumberColumnFilter' },
    { headerName: 'Elevation', field: config.Elevation, width: 200, filter: 'agNumberColumnFilter' },
    { headerName: 'Flat No.', field: config.Flat_no, width: 200, filter: 'agNumberColumnFilter' },
    { headerName: 'Floor No.', field: config.Floor_no, width: 250, filter: 'agNumberColumnFilter' },
    { headerName: 'Heading', field: config.Heading, width: 150, filter: 'agNumberColumnFilter' },
    { headerName: 'Unit Id', field: config.Uni_Id, width: 250, filter: 'agNumberColumnFilter' },
    // { headerName: 'OBJECTID', field: 'OBJECTID', width: 250 },
    {
      headerName: '',
      field: 'geometry',
      width: 100,
      cellRenderer: lineOfSightRenderer,
      cellRendererParams: {
        jimuMapView: jimuMapView,
        widgetProps: props,
        clicked: function (field) {
          alert(`${field} was clicked`)
        }
      }
    },
    {
      headerName: '',
      field: 'geometry',
      width: 100,
      cellRenderer: cameraViewRenderer,
      cellRendererParams: {
        jimuMapView: jimuMapView,
        widgetProps: props,
        clicked: function (field) {
          alert(`${field} was clicked`)
        }
      }
    }
  ]

  const rowData 
  = [
    {
      customerName: 'Esraa',
      customerNumber: null,
      fieldName: "Current_Consumption",
      id: 8883,
      isRead: false,
      message: "Current_Consumption",
      recordId: 9,
      tableName: "Gas_Meter",
      value: 0.008
    },
    {
      customerName: 'Linda',
      customerNumber: null,
      fieldName: "Current_Consumption",
      id: 8883,
      isRead: false,
      message: "Current_Consumption",
      recordId: 9,
      tableName: "Gas_Meter",
      value: 0.008
    },
    {
      customerName: 'Nermeen',
      customerNumber: null,
      fieldName: "Current_Consumption",
      id: 8883,
      isRead: false,
      message: "Current_Consumption",
      recordId: 9,
      tableName: "Gas_Meter",
      value: 0.008
    },
  ];

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  const defaultColDef = useMemo(() => {
    return {
      sortable: true,
      sortingOrder: ['desc', 'asc', null] 
    };
  }, []);

  return (
    <div className="widget-demo jimu-widget m-2 ag-theme-alpine">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      <AgGridReact
      columnDefs={columnDefs}
      defaultColDef={defaultColDef}
      rowData={gridData}
      // alwaysShowHorizontalScroll={true}
      alwaysShowVerticalScroll={true}
      pagination={true}
      sortingOrder={['desc']}
      />
    </div>
  )
}

export default Widget
