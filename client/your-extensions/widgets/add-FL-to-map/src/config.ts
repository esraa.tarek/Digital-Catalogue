import { ImmutableObject } from 'seamless-immutable'

export interface Config {
  featureLayerURL: string
}

export type IMConfig = ImmutableObject<Config>
