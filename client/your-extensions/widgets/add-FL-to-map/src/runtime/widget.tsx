import { React, AllWidgetProps } from 'jimu-core'
import config from '../../config.json'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis';
import FeatureLayer from 'esri/layers/FeatureLayer';

const {useState, useEffect} = React;

const Widget = (props: AllWidgetProps<any>) => {

  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  useEffect(()=>{
    if(jimuMapView){
      // create a new FeatureLayer
      const layer = new FeatureLayer({
        url: config.featureLayerURL
      });
  
      // Add the layer to the map (accessed through the Experience Builder JimuMapView data source)
      jimuMapView.view.map.add(layer);
    }

  },[jimuMapView]);

  const formSubmit = (evt) => {
    evt.preventDefault()
  
  }

  return (
    <div className="widget-demo jimu-widget m-2">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
    </div>
  )
}

export default Widget
