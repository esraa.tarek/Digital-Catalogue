import { React, AllWidgetProps, IMState, Immutable } from 'jimu-core'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis'
import projection from 'esri/geometry/projection'

import CameraRotationIcon from '../images/rotate-icon.png'
import CameraRotateLeftIcon from '../images/rotate-left.png'
import CameraRotateRightIcon from '../images/rotate-right.png'

import TiltIcon from '../images/tilt-icon.png'
import FovIcon from '../images/FOV.png'

import PlusIcon from '../images/plus.png'
import MinusIcon from '../images/minus.png'

import config from '../../config.json'
import './widget.scss'
import Expand from 'esri/widgets/Expand'

const { useState, useEffect } = React

const Widget = (props: AllWidgetProps<any> & { state: IMState }, any) => {

  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()
  const [transform, setTransform] = useState()
  const [selectedCameraViewGridItem, setSelectedCameraViewGridItem] = useState<Graphic>()

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  useEffect(() => {
    if (props.stateProps) {
      setSelectedCameraViewGridItem(props.stateProps.selectedCameraViewGraphic)
    }
  }, [props.stateProps?.selectedCameraViewGraphic])

  useEffect(() => {
    if (selectedCameraViewGridItem) {
      cameraView()
    }
  }, [selectedCameraViewGridItem])

  useEffect(() => {
    if (jimuMapView) {
      renderCameraViewWidgetUI();
      
      // Watch the change on view.camera
      jimuMapView.view.watch('camera', updateIndicator)
    }
  }, [jimuMapView])

  const renderCameraViewWidgetUI = () => {
    // add an Expand widget to make the menu responsive
    const expand = new Expand({
     expandTooltip: "Expand Camera View widget",
     view: jimuMapView.view,
     content: document.getElementById("cameraViewExpandContainer"),
     collapseIconClass: "esri-icon-hollow-eye",
     expandIconClass: "esri-icon-hollow-eye",
     // expanded: true //makes widget expanded by default
   });
   
   jimuMapView.view.ui.add(expand, "bottom-right");

   //handle rotation btns click actions
   document.getElementById("rotateLeftIcon").addEventListener("click", () => rotateView(-1));
   document.getElementById("rotateRightIcon").addEventListener("click", () => rotateView(1));
   document.getElementById("tiltSpan").addEventListener("click", tiltView);

   //handle tilt btns click actions
   document.getElementById("increaseTiltIcon").addEventListener("click", onIncreaseTilt);
   document.getElementById("decreaseTiltIcon").addEventListener("click", onDecreaseTilt);

   //handle fov btns click actions
   document.getElementById("increaseFOVIcon").addEventListener("click", ()=>{onChangeFov("PLUS")});
   document.getElementById("decreaseFOVIcon").addEventListener("click", ()=>{onChangeFov("MINUS")});
 }

  const cameraView = () => {
    const heading = selectedCameraViewGridItem.attributes[config.headingAttribute]
    console.log(heading)
    
    projection.load().then(function () {
      const projectedGeometry = projection.project(selectedCameraViewGridItem.geometry, { wkid: 32636 })
      jimuMapView.view.camera.position = projectedGeometry;
      jimuMapView.view.camera.position.z = selectedCameraViewGridItem.attributes[config.elevationAttribute];
      jimuMapView.view.camera.heading = heading;
      jimuMapView.view.camera.tilt = 80;

      const cam = jimuMapView.view.camera.clone();
      cam.fov = 135;
      jimuMapView.view.camera = cam;

      jimuMapView.view.goTo(jimuMapView.view.camera).catch((error) => {
        if (error.name !== 'AbortError') {
          console.error(error)
        }
      });
    })
  }

  const rotateView = (direction) => {
    let heading = jimuMapView.view.camera.heading

    // Set the heading of the view to the closest multiple of 90 degrees,
    // depending on the direction of rotation
    if (direction > 0) {
      heading = heading + 10;
      // heading = Math.floor((heading + 1e-3) / 10) * 10 + 10
    } else {
      heading = heading - 10;
      // heading = Math.ceil((heading - 1e-3) / 10) * 10 - 10
    }

    jimuMapView.view.goTo({
      heading: heading
    }).catch((error) => {
      if (error.name != 'AbortError') {
        console.error(error)
      }
    })
  }

  const tiltView = () => {
    // Get the camera tilt and add a small number for numerical inaccuracies
    let tilt = jimuMapView.view.camera.tilt + 1e-3

    // Switch between 3 levels of tilt
    if (tilt >= 80) {
      tilt = 0
    } else if (tilt >= 40) {
      tilt = 80
    } else {
      tilt = 40
    }

    jimuMapView.view.goTo({
      tilt: tilt
    }).catch((error) => {
      if (error.name != 'AbortError') {
        console.error(error)
      }
    })
  }

  const updateIndicator = (camera) => {
    const tilt = camera.tilt
    const heading = camera.heading
    // Update the indicator to reflect the current tilt/heading using
    // css transforms.

    const transform = {
      transform: `rotateX(${
      0.8 * tilt
    }deg) rotateY(0) rotateZ(${-heading}deg)`
    }

    setTransform(transform)
  }

  const onChangeFov = (value) => {
    const cam = jimuMapView.view.camera.clone();

    if(value === "PLUS"){
      cam.fov += 10;
    }else if(value === "MINUS"){
      cam.fov -= 10;
    }
    
    jimuMapView.view.camera = cam;

    jimuMapView.view.goTo(jimuMapView.view.camera).catch((error) => {
      if (error.name !== 'AbortError') {
        console.error(error)
      }
    })
  }

  const onIncreaseTilt = () => {
      const cam = jimuMapView.view.camera.clone();
      jimuMapView.view.camera.tilt = cam.tilt+=10;
     
      jimuMapView.view.goTo(cam).catch((error) => {
        if (error.name !== 'AbortError') {
          console.error(error)
        }
      })
      
    }

    const onDecreaseTilt = () => {
      const cam = jimuMapView.view.camera.clone();
      jimuMapView.view.camera.tilt = cam.tilt-=10;
     
      jimuMapView.view.goTo(cam).catch((error) => {
        if (error.name !== 'AbortError') {
          console.error(error)
        }
      })
      
    }

  return (
    <div className="widget-demo jimu-widget mt-2 height-100">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      <div id="cameraViewExpandContainer">
        {/* <h3>Camera Controls</h3> */}
        <table id="cameraViewContainer">

            <tr>
              <td>
                <img className='camera-view-rotate_icon mainIcon' src={CameraRotationIcon} title="Camera Rotation" />
              </td>
            </tr>
            <tr className='row-2'>
              <td>
                <img id="rotateLeftIcon" className='camera-controls_icon' src={CameraRotateLeftIcon} title="Rotate 10°" />
                <button id="tiltSpan" style={transform} title="Change Tilt"></button>
                <img id="rotateRightIcon" className='camera-controls_icon ml-0' src={CameraRotateRightIcon} title="Rotate 10°" />
              </td>
            </tr>

            <tr>
              <td><img className='camera-view-rotate_icon mainIcon' src={TiltIcon} title="Camera Tilt" /></td>
            </tr>
            <tr className='row-2 tiltContainer'>
              <td>
                <img id="increaseTiltIcon" className='camera-controls_icon' src={PlusIcon} title="Increase tilt" />
                <img id="decreaseTiltIcon" className='camera-controls_icon' src={MinusIcon} title="Decrease tilt" />
              </td>
            </tr>
        
            <tr>
              <td><img className='camera-view-rotate_icon mainIcon' src={FovIcon} title="Camera FOV" /></td>
            </tr>
            <tr  className='row-2 fovContainer'>
              <td>
                <img id="increaseFOVIcon" className='camera-controls_icon' src={PlusIcon} title="Increase FOV" />
                <img id="decreaseFOVIcon" className='camera-controls_icon' src={MinusIcon} title="Decrease FOV" />
              </td>
            </tr>

        </table>
      </div>
    </div>
  )
}

export default Widget
