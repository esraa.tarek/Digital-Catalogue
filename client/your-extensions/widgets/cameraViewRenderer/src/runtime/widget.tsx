import { React, AllWidgetProps, appActions, Immutable } from 'jimu-core'
import { IMConfig } from '../config'

import CameraIcon from './camera.png'
import './widget.scss'

const Widget = (props: AllWidgetProps<any>) => {
  const showCameraView = () => {
    console.log(props.data.attributes) //geometry

    const sr = Immutable.asMutable(props.data.geometry.spatialReference)

    props.widgetProps.dispatch(
      appActions.widgetStatePropChange(
         'widget_237', //'widget_235', //'widget_232', //id of camera-view widget .. should be changed if the widget removed and re-added using exp builder
        'selectedCameraViewGraphic',
        {
          attributes: props.data.attributes,
          geometry: {
            x: props.data.geometry.x,
            y: props.data.geometry.y,
            z: props.data.geometry.z,
            spatialReference: sr
          }
        })
    )
  }

  return (
    <button className='camera-view-renderer_btn' onClick={showCameraView}><span>
      <img className='camera-view-renderer_icon' src={CameraIcon} title="Camera view" />
      </span></button>
  )
}

export default Widget
