import { React, AllWidgetProps, IMState, Immutable } from 'jimu-core'
import config from '../../config.json'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis';
import FeatureLayer from 'esri/layers/FeatureLayer';
import LineOfSight from "esri/widgets/LineOfSight";
import Graphic from "esri/Graphic";
import Point from 'esri/geometry/Point';
import Expand from 'esri/widgets/Expand';
import geodesicUtils from 'esri/geometry/support/geodesicUtils'

import './widget.scss';

const {useState, useEffect} = React;

const Widget = (props: AllWidgetProps<any>, any) => {

  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()
  const [selectedLineOfSightGridItem, setSelectedLineOfSightGridItem] = useState<Graphic>()
  const [losViewModel, setLosViewModel] = useState(null)
  const distance = 200;

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  useEffect(()=>{
    if(props.stateProps){
      setSelectedLineOfSightGridItem(props.stateProps.selectedLineOfSightGraphic)
    }
  },[props.stateProps?.selectedLineOfSightGraphic]);

  useEffect(()=>{
    if(selectedLineOfSightGridItem){
      drawLineOfSightForSelectedItem();
    }
  },[selectedLineOfSightGridItem]);

  useEffect(()=>{
    if(jimuMapView){
      addPolygonFL();

      jimuMapView.view.on("click", function(event) {
        // Get the coordinates of the click on the view
        var lat = Math.round(event.mapPoint.latitude * 1000) / 1000;
        var lon = Math.round(event.mapPoint.longitude * 1000) / 1000;

        var x = event.mapPoint.x;
        var y = event.mapPoint.y;

        console.log("lat/lon: ", lat, lon);
        console.log("x/y: ", x, y);
      });
      
      //Line of Sight
      const lineOfSight = new LineOfSight({
        view: jimuMapView.view,
        container: "losWidget"
      });

      /**************************************
         * Add symbols to mark the intersections
         * for the line of sight
         **************************************/

        const viewModel = lineOfSight.viewModel;
        setLosViewModel(viewModel);

      /**************************************
         * Create an analysis by setting
         * the initial observer and four targets
         **************************************/

      // viewModel.observer = new Point({
      //   latitude: 30.0520215,
      //   longitude: 31.2329916,
      //   z: 32,
      //   spatialReference: {wkid: 4326}
      //   // x: 329667.60212451685,
      //   // y: 3325923.077073421,
      //   // // latitude: 30.052, // 30.052 31.234
      //   // // longitude: 31.234,
      //   // z: 46.540474137682935,
      //   // spatialReference: {wkid: 32636}
      // });

      // const target_1 = geodesicUtils.pointFromDistance(viewModel.observer, 200, 175);

      // viewModel.targets = [
      //   {
      //     location: target_1
      //   }
      //   // createTarget(329637.4402215136, 3325830.497136809, 0),
      //   // createTarget(329543.56750109605, 3325876.948998765, 0),
      //   // createTarget(329597.2623614868, 3325848.8141317274, 0),
      // ];

      renderLineOfSightWidgetUI();

    }

  },[jimuMapView]);

  const drawLineOfSightForSelectedItem = () =>{
    const geometry = Immutable.asMutable(selectedLineOfSightGridItem.geometry);
    console.log(geometry);
    //observer point
    losViewModel.observer = new Point({
      latitude: selectedLineOfSightGridItem.geometry.y,
      longitude: selectedLineOfSightGridItem.geometry.x,
      z: selectedLineOfSightGridItem.attributes[config.elevationAttribute],
      spatialReference: selectedLineOfSightGridItem.geometry.spatialReference
    });

    //target points
    const mainTarget = geodesicUtils.pointFromDistance(losViewModel.observer, distance, selectedLineOfSightGridItem.attributes[config.headingAttribute]);

    const targets = [];
    for (let i = 5; i <= 45; i+=5) {
      const heading = selectedLineOfSightGridItem.attributes[config.headingAttribute];
      let plusHeading;
      let minusHeading;

      //example:
      //problem: 340 + 45 = 385 > 360 -> must be: 25 deg
      // x = 360 - 340 = 20
      // 45 - x = 25

      //problem: 30 - 45 = -15 -> must be: 345 deg
      // x = 45 - 30 = 15
      // 360 - x = 345

      if(heading + i > 360){
        const delta = 360 - heading;
        const newAngle = i - delta;
        plusHeading = newAngle;
      }else if(heading - i < 0){
        const delta = i - heading;
        const newAngle = 360 - delta;
        minusHeading = newAngle;
      }

      const target_1 = geodesicUtils.pointFromDistance(losViewModel.observer, distance, plusHeading ? plusHeading : heading + i);
      const target_2 = geodesicUtils.pointFromDistance(losViewModel.observer, distance, minusHeading ? minusHeading : heading - i);
      targets.push({ location: target_1 },{ location: target_2 });
    }
    targets.push({ location: mainTarget });
    
    losViewModel.targets = [...targets];

  }

  const addPolygonFL = () => {
     // create a new FeatureLayer
     const layer = new FeatureLayer({
      url: config.polygonsFL
    });

    // Add the layer to the map (accessed through the Experience Builder JimuMapView data source)
    jimuMapView.view.map.add(layer);
  }

  const renderLineOfSightWidgetUI = () => {
     // add an Expand widget to make the menu responsive
     const expand = new Expand({
      expandTooltip: "Expand line of sight widget",
      view: jimuMapView.view,
      content: document.getElementById("lineOfSightContainer"),
      collapseIconClass: "esri-icon-line-of-sight",
      expandIconClass: "esri-icon-line-of-sight",
      // expanded: true //makes widget expanded by default
    });
    
    jimuMapView.view.ui.add(expand, "bottom-right");
  }

  const createTarget = (lat, lon, z) => {
    return {
      location: new Point({
        x: lat,
        y: lon,
        z: z || 0,
        spatialReference: {wkid: 32636}
      })
    };
  }

  const setIntersectionMarkers = (viewModel) => {
    jimuMapView.view.graphics.removeAll();

    const intersectionSymbol = {
      type: "point-3d",
      symbolLayers: [
        {
          type: "object",
          resource: { primitive: "inverted-cone" },
          material: { color: [255, 100, 100] },
          height: 2,
          depth: 2,
          width: 2,
          anchor: "relative",
          anchorPosition: { x: 0, y: 0, z: -0.7 }
        }
      ]
    };
    
    viewModel.targets.forEach((target) => {
      if (target.intersectedLocation) {
        const graphic = new Graphic({
          symbol: intersectionSymbol,
          geometry: target.intersectedLocation
        });
        jimuMapView.view.graphics.add(graphic);
      }
    });
  }

  return (
    <div className="widget-demo jimu-widget m-2">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      <div id="lineOfSightContainer">
        <h3>Line of sight analysis</h3>
        <div id="losWidget"></div>
      </div>
    </div>
  )
}

export default Widget
