import { ImmutableObject } from 'seamless-immutable'

export interface Config {
  polygonsFL: string
}

export default Config

export type IMConfig = ImmutableObject<Config>
