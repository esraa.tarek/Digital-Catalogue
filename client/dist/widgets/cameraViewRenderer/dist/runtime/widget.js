System.register(["jimu-core"], function(__WEBPACK_DYNAMIC_EXPORT__, __system_context__) {
	var __WEBPACK_EXTERNAL_MODULE_jimu_core__ = {};
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_jimu_core__, "__esModule", { value: true });
	return {
		setters: [
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_jimu_core__[key] = module[key];
				});
			}
		],
		execute: function() {
			__WEBPACK_DYNAMIC_EXPORT__(
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[3].use[1]!./node_modules/resolve-url-loader/index.js??ruleSet[1].rules[3].use[2]!./node_modules/sass-loader/dist/cjs.js??ruleSet[1].rules[3].use[3]!./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss":
/*!****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[3].use[1]!./node_modules/resolve-url-loader/index.js??ruleSet[1].rules[3].use[2]!./node_modules/sass-loader/dist/cjs.js??ruleSet[1].rules[3].use[3]!./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/sourceMaps.js */ "./node_modules/css-loader/dist/runtime/sourceMaps.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1__);
// Imports


var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_1___default()((_node_modules_css_loader_dist_runtime_sourceMaps_js__WEBPACK_IMPORTED_MODULE_0___default()));
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".camera-view-renderer_btn {\n  border: none;\n  background-color: transparent;\n  width: 100%;\n  padding: 0;\n}\n.camera-view-renderer_icon {\n  width: 25px;\n  height: auto;\n}", "",{"version":3,"sources":["webpack://./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss","webpack://./../../../../Experience%20Builder/Digital%20Catalogue%20git/Digital-Catalogue/client/your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss"],"names":[],"mappings":"AACI;EACI,YAAA;EACA,6BAAA;EACA,WAAA;EACA,UAAA;ACAR;ADEI;EACI,WAAA;EACA,YAAA;ACAR","sourcesContent":[".camera-view-renderer{\r\n    &_btn{\r\n        border: none;\r\n        background-color: transparent;\r\n        width: 100%;\r\n        padding: 0;\r\n    }\r\n    &_icon{\r\n        width: 25px;\r\n        height: auto;\r\n    }\r\n}",".camera-view-renderer_btn {\n  border: none;\n  background-color: transparent;\n  width: 100%;\n  padding: 0;\n}\n.camera-view-renderer_icon {\n  width: 25px;\n  height: auto;\n}"],"sourceRoot":""}]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
module.exports = function (cssWithMappingToString) {
  var list = [];

  // return the list of modules as css string
  list.toString = function toString() {
    return this.map(function (item) {
      var content = "";
      var needLayer = typeof item[5] !== "undefined";
      if (item[4]) {
        content += "@supports (".concat(item[4], ") {");
      }
      if (item[2]) {
        content += "@media ".concat(item[2], " {");
      }
      if (needLayer) {
        content += "@layer".concat(item[5].length > 0 ? " ".concat(item[5]) : "", " {");
      }
      content += cssWithMappingToString(item);
      if (needLayer) {
        content += "}";
      }
      if (item[2]) {
        content += "}";
      }
      if (item[4]) {
        content += "}";
      }
      return content;
    }).join("");
  };

  // import a list of modules into the list
  list.i = function i(modules, media, dedupe, supports, layer) {
    if (typeof modules === "string") {
      modules = [[null, modules, undefined]];
    }
    var alreadyImportedModules = {};
    if (dedupe) {
      for (var k = 0; k < this.length; k++) {
        var id = this[k][0];
        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }
    for (var _k = 0; _k < modules.length; _k++) {
      var item = [].concat(modules[_k]);
      if (dedupe && alreadyImportedModules[item[0]]) {
        continue;
      }
      if (typeof layer !== "undefined") {
        if (typeof item[5] === "undefined") {
          item[5] = layer;
        } else {
          item[1] = "@layer".concat(item[5].length > 0 ? " ".concat(item[5]) : "", " {").concat(item[1], "}");
          item[5] = layer;
        }
      }
      if (media) {
        if (!item[2]) {
          item[2] = media;
        } else {
          item[1] = "@media ".concat(item[2], " {").concat(item[1], "}");
          item[2] = media;
        }
      }
      if (supports) {
        if (!item[4]) {
          item[4] = "".concat(supports);
        } else {
          item[1] = "@supports (".concat(item[4], ") {").concat(item[1], "}");
          item[4] = supports;
        }
      }
      list.push(item);
    }
  };
  return list;
};

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/sourceMaps.js":
/*!************************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/sourceMaps.js ***!
  \************************************************************/
/***/ ((module) => {

"use strict";


module.exports = function (item) {
  var content = item[1];
  var cssMapping = item[3];
  if (!cssMapping) {
    return content;
  }
  if (typeof btoa === "function") {
    var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(cssMapping))));
    var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
    var sourceMapping = "/*# ".concat(data, " */");
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || "").concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join("\n");
  }
  return [content].join("\n");
};

/***/ }),

/***/ "./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss":
/*!****************************************************************************!*\
  !*** ./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/styleDomAPI.js */ "./node_modules/style-loader/dist/runtime/styleDomAPI.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/insertBySelector.js */ "./node_modules/style-loader/dist/runtime/insertBySelector.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js */ "./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/insertStyleElement.js */ "./node_modules/style-loader/dist/runtime/insertStyleElement.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/styleTagTransform.js */ "./node_modules/style-loader/dist/runtime/styleTagTransform.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_3_use_1_node_modules_resolve_url_loader_index_js_ruleSet_1_rules_3_use_2_node_modules_sass_loader_dist_cjs_js_ruleSet_1_rules_3_use_3_widget_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[3].use[1]!../../../../../node_modules/resolve-url-loader/index.js??ruleSet[1].rules[3].use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??ruleSet[1].rules[3].use[3]!./widget.scss */ "./node_modules/css-loader/dist/cjs.js??ruleSet[1].rules[3].use[1]!./node_modules/resolve-url-loader/index.js??ruleSet[1].rules[3].use[2]!./node_modules/sass-loader/dist/cjs.js??ruleSet[1].rules[3].use[3]!./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss");

      
      
      
      
      
      
      
      
      

var options = {};

options.styleTagTransform = (_node_modules_style_loader_dist_runtime_styleTagTransform_js__WEBPACK_IMPORTED_MODULE_5___default());
options.setAttributes = (_node_modules_style_loader_dist_runtime_setAttributesWithoutAttributes_js__WEBPACK_IMPORTED_MODULE_3___default());

      options.insert = _node_modules_style_loader_dist_runtime_insertBySelector_js__WEBPACK_IMPORTED_MODULE_2___default().bind(null, "head");
    
options.domAPI = (_node_modules_style_loader_dist_runtime_styleDomAPI_js__WEBPACK_IMPORTED_MODULE_1___default());
options.insertStyleElement = (_node_modules_style_loader_dist_runtime_insertStyleElement_js__WEBPACK_IMPORTED_MODULE_4___default());

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_3_use_1_node_modules_resolve_url_loader_index_js_ruleSet_1_rules_3_use_2_node_modules_sass_loader_dist_cjs_js_ruleSet_1_rules_3_use_3_widget_scss__WEBPACK_IMPORTED_MODULE_6__["default"], options);




       /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_3_use_1_node_modules_resolve_url_loader_index_js_ruleSet_1_rules_3_use_2_node_modules_sass_loader_dist_cjs_js_ruleSet_1_rules_3_use_3_widget_scss__WEBPACK_IMPORTED_MODULE_6__["default"] && _node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_3_use_1_node_modules_resolve_url_loader_index_js_ruleSet_1_rules_3_use_2_node_modules_sass_loader_dist_cjs_js_ruleSet_1_rules_3_use_3_widget_scss__WEBPACK_IMPORTED_MODULE_6__["default"].locals ? _node_modules_css_loader_dist_cjs_js_ruleSet_1_rules_3_use_1_node_modules_resolve_url_loader_index_js_ruleSet_1_rules_3_use_2_node_modules_sass_loader_dist_cjs_js_ruleSet_1_rules_3_use_3_widget_scss__WEBPACK_IMPORTED_MODULE_6__["default"].locals : undefined);


/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/***/ ((module) => {

"use strict";


var stylesInDOM = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDOM.length; i++) {
    if (stylesInDOM[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var indexByIdentifier = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3],
      supports: item[4],
      layer: item[5]
    };

    if (indexByIdentifier !== -1) {
      stylesInDOM[indexByIdentifier].references++;
      stylesInDOM[indexByIdentifier].updater(obj);
    } else {
      var updater = addElementStyle(obj, options);
      options.byIndex = i;
      stylesInDOM.splice(i, 0, {
        identifier: identifier,
        updater: updater,
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function addElementStyle(obj, options) {
  var api = options.domAPI(options);
  api.update(obj);

  var updater = function updater(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap && newObj.supports === obj.supports && newObj.layer === obj.layer) {
        return;
      }

      api.update(obj = newObj);
    } else {
      api.remove();
    }
  };

  return updater;
}

module.exports = function (list, options) {
  options = options || {};
  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDOM[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDOM[_index].references === 0) {
        stylesInDOM[_index].updater();

        stylesInDOM.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertBySelector.js":
/*!********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertBySelector.js ***!
  \********************************************************************/
/***/ ((module) => {

"use strict";


var memo = {};
/* istanbul ignore next  */

function getTarget(target) {
  if (typeof memo[target] === "undefined") {
    var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

    if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
      try {
        // This will throw an exception if access to iframe is blocked
        // due to cross-origin restrictions
        styleTarget = styleTarget.contentDocument.head;
      } catch (e) {
        // istanbul ignore next
        styleTarget = null;
      }
    }

    memo[target] = styleTarget;
  }

  return memo[target];
}
/* istanbul ignore next  */


function insertBySelector(insert, style) {
  var target = getTarget(insert);

  if (!target) {
    throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
  }

  target.appendChild(style);
}

module.exports = insertBySelector;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/insertStyleElement.js":
/*!**********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/insertStyleElement.js ***!
  \**********************************************************************/
/***/ ((module) => {

"use strict";


/* istanbul ignore next  */
function insertStyleElement(options) {
  var element = document.createElement("style");
  options.setAttributes(element, options.attributes);
  options.insert(element, options.options);
  return element;
}

module.exports = insertStyleElement;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/setAttributesWithoutAttributes.js ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


/* istanbul ignore next  */
function setAttributesWithoutAttributes(styleElement) {
  var nonce =  true ? __webpack_require__.nc : 0;

  if (nonce) {
    styleElement.setAttribute("nonce", nonce);
  }
}

module.exports = setAttributesWithoutAttributes;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleDomAPI.js":
/*!***************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleDomAPI.js ***!
  \***************************************************************/
/***/ ((module) => {

"use strict";


/* istanbul ignore next  */
function apply(styleElement, options, obj) {
  var css = "";

  if (obj.supports) {
    css += "@supports (".concat(obj.supports, ") {");
  }

  if (obj.media) {
    css += "@media ".concat(obj.media, " {");
  }

  var needLayer = typeof obj.layer !== "undefined";

  if (needLayer) {
    css += "@layer".concat(obj.layer.length > 0 ? " ".concat(obj.layer) : "", " {");
  }

  css += obj.css;

  if (needLayer) {
    css += "}";
  }

  if (obj.media) {
    css += "}";
  }

  if (obj.supports) {
    css += "}";
  }

  var sourceMap = obj.sourceMap;

  if (sourceMap && typeof btoa !== "undefined") {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  options.styleTagTransform(css, styleElement, options.options);
}

function removeStyleElement(styleElement) {
  // istanbul ignore if
  if (styleElement.parentNode === null) {
    return false;
  }

  styleElement.parentNode.removeChild(styleElement);
}
/* istanbul ignore next  */


function domAPI(options) {
  var styleElement = options.insertStyleElement(options);
  return {
    update: function update(obj) {
      apply(styleElement, options, obj);
    },
    remove: function remove() {
      removeStyleElement(styleElement);
    }
  };
}

module.exports = domAPI;

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/styleTagTransform.js":
/*!*********************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/styleTagTransform.js ***!
  \*********************************************************************/
/***/ ((module) => {

"use strict";


/* istanbul ignore next  */
function styleTagTransform(css, styleElement) {
  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css;
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild);
    }

    styleElement.appendChild(document.createTextNode(css));
  }
}

module.exports = styleTagTransform;

/***/ }),

/***/ "./your-extensions/widgets/cameraViewRenderer/src/runtime/camera.png":
/*!***************************************************************************!*\
  !*** ./your-extensions/widgets/cameraViewRenderer/src/runtime/camera.png ***!
  \***************************************************************************/
/***/ ((module) => {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7d152F1VebDxO3OYwhBmQkiQEGZEEFEQECiDgDjiVJEqiHW2DtjWrw7VijNQq8XW1lJbZVCroDIoIDJIEZR5lCQQJhkCIQyZvz+e8zYhJOGc856119573b/req43gWTt5+yzc9Zz1l57LZAkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkSZIkqSVG5E5AaqBNgenAtsBWwARgLWCdnEkV5AngSWAuMBO4A7gVeDBjTlLjWABIz29D4Ehgf2A/otNX/cwEfg1cApwDPJIzGUlSM40BjiY6kgXAUqNRMR/4KfCGznspSdJqjQOOIYaVc3dixmBiJvBBYE0kSVrBCOBtwP3k77CMNHEv8Fa89SlJ6tgeuJT8HZRRTVwCbIckqWjHAPPI3ykZ1cZTxG0BSVJhxgGnk78jMvLGd4lrQZJUgLWB88nf+Rj1iIuBdZEktdrGwLXk73SMesW1xLUhFcPZsCrJxsCvgJ1yJ6JauhU4gHgSRGo9CwCVws5f3bAIUDEsAFQCO3/1wiJARbAAUNvZ+asfFgFqPQsAtZmdv4bDIkCtZgGgtrLz1yBYBKi1LADURnb+GiSLALWSBYDaxs5fKVgEqHUsANQmdv5KySJArWIBoLaw81cVLALUGhYAagM7f1XJIkCtYAGgprPzVw4WAWo8CwA1mZ2/crIIUKNZAKip7PxVBxYBaiwLADWRnb/qxCJAjWQBoKax81cdWQSocSwA1CR2/qoziwA1igWAmsLOX01gEaDGsABQE9j5q0ksAtQIFgCqOzt/NZFFgGrPAkB1ZuevJrMIUK1ZAKiu7PzVBhYBqi0LANWRnb/axCJAtWQBoLqx81cbWQSodiwAVCd2/moziwDVigWA6sLOXyWwCFBtWACoDuz8VRKLANWCBYBys/NXiSwClJ0FgHKy81fJLAKU1cjcCahYmwAXYeevcm0HXEAUwlLlLACUwy7AlcCOuRORMtsJuAbYPXciKs+o3AmoKGsDnwD+Hb/1SEMmAG8lvpBdCyzIm45K4RwApbQGsAUx1HkE8Fpgo6wZSfX2EPAj4BxijsB9wNNZM1JrWQDUwzhgZ2BborPcFpgCrEt8a14LWD9XcpLUoznAPOBJ4HFgJnAbUdTcDtwIzM+VnIIFQB6jgBcDryBmAe9NfFuWpBI8BVxOTAS+GLgaWJI1owJZAFRre+CNwNuJb/iSpLjVcTbwXeD3eVMphwVAemOAtwAfAF6UORdJqrtrgFOA7wOLMufSahYA6YwF3gR8EpiWORdJappZwNeBb+NEyCQsANJ4E/AVYga8JKl/9wAfAc7KnUjbWAAM1jbAN4BDciciSS1zMfBe4JbcibSFCwENxkhigZuzgOmZc5GkNpoKHEcslHRF5lxawRGA4dsQOB04LHciklSIXwJ/DjyYO5EmswAYnpcDPwA2z52IJBVmNvFYtaMBfXIzoP4dRezkZecvSdWbRCwk9PrciTSVcwD6cyzwPeJRP0lSHqOB1xG3Aq7JnEvjWAD07kRipr+jJ5KU30jgcGJ5YW8H9MACoDcnACfnTkKS9CwjgIOAe4ktldUFC4DuHQX8B37zl6Q6GkGMBNyEawV0xacAuvNy4EJi215JUn09Q+yyemXuROrOAuD5bUTsTuWyvpLUDLOB3YCHcydSZw5nr94I4DvY+UtSk0wiFmjzS+5qOAdg9T4BvCd3EpKknk0DnsQnA1bJ6mjVpgE34H1/SWqq+cCuwG25E6kjbwGs2inY+UtSk40DTs2dRF1ZAKzcm3BzH0lqg4OJ1QK1Am8BPNd44E6c+CdJbXE3cVt3Qe5E6sQRgOf6C+z8JalNJgNvz51E3TgC8GxjiMkiU3MnIkkaqLuA6cCi3InUhSMAz/YW7PwlqY22JuZ3qcMRgGe7BnhR7iQkSUlcDeyZO4m6sABYZgdiEwlJUnvthJ/1gLcAlucEEUlqv7fkTqAuHAEII4FZxPrRkqT2uhfYClicO5HcHAEIe2LnL0kl2AJ4Ye4k6sACIByQOwFJUmUOzJ1AHVgAhFfkTkCSVBk/83EOAMBYYA6wZu5EJEmVeArYgNgtsFiOAMDO2PlLUknWJB79LpoFAGyXOwFJUuWm504gNwsALwJJKlHxn/0WALBt7gQkSZUr/rPfAgCm5E5AklS5rXMnkJsFAKybOwFJUuUm5E4gNwsAWCd3ApKkyhX/2W8B4EUgSSUq/rPfhYBgETAqdxJSQo8DD3V+PgYsBRYC8zr/f21gDPF5sB5xW2wjvD3WRs8AdwIPAE92/ttawKbANGBcprxyWAyMzp1EThYA8WEoNdnDwHXAbcAMYGYn7gEeIYrcfowGJgKTicmyQzEd2BXYsN+EVZkFwM+B84BLgDuAJav4syOJmfGvAA4BDiNWSm2zovvAol98hwWAmuRR4ErgCuAa4Hrg/ky5bE6spLkH8FLgZcD6mXLRs90LfA34LnHN9GMicCzwV8R73UZF94FFv/gOCwDV2TzgV8Q3uEuBW6jvNTsS2B7Yl/j2eAAxvKzqzAU+DXyTwa1zPw54P/Ap4nZRmxTdBxb94jvq+mGqct0DnA2cC1xGDOM20Tjg5cDhwOuBSXnTab0LiW/s9yVqfxLwH7Rr+3T7wMItNYwaxP3AKcDetPNDaSRRDPwjMQEt9/luUywBPkM1T3WNAj5X8etLGSpc7gvQKDcWExO0Xk1Zs5HHAK8DfkGcg9zvQ5NjEXB8b6d/IE7oHDv36x9uqHC5L0CjvHgE+Adidn3ppgBfICaq5X5fmhZLyNP5DzlhFXk1KVS43BegUU7cCbwPJ8atzNrAB4C7yP8+NSU+09eZHqym3w5Q4XJfgEb74w7gz3HBqW6MAo4hiqXc71ud4wLqsZLrKOAi8p+PfkOFy30BGu2NmcA7Kev+/qCMIYa3Z5H/faxbPE69nsvfEniC/Oeln1Dhcl+ARvtiLnAiZS2rmsp44K9pbgeTIj48rDOaxsfJf176CRUu9wVotCeWAKcT66prsDYDTqMdM8+HE/dQz8JyPLH6YO7z02uocLkvQKMdcSOwF0ptb+Bm8r/fuaKO3/6HfJT856fXUOFyX4BGs2MBcBL1/FbWVmOIWyzPkP/9rzLmU+8NmCYSOeY+T72ECpf7AjSaG78HdkS57Ezsgpj7OqgqfjyY05bUOeQ/T71E0erwGInUNEuBU4kh/5sy51KyG4A9gS+y6i1u2+S83Al0oQk5Sv8ndwVqNCvuBw5CdXMI8CD5r4+UMX1gZyudHch/nnqJorVx05FeFX8RqGuXA0eTbrc1Dc8WwFnAS3MnksAzxGqJi3Mn8jxGEVtYj8+dSJeK7gO9BSB15xvENqh2/vV1L7A/8K3MeaRwB/Xv/CFy/GPuJNQdCwBp9RYCxwHvJ2b8q94WAO8B3kWsGdAWD+VOoAd/yp2AumMBIK3aPOAo4Du5E1HP/gU4jFg2tw2eyJ1AD5qUa9Fco1xauXuAw4mZ5k0yAphKTMaaSmy3OxnYiHhOeyJxf3Ysy3YlfJL45vwMsVXxI8S3uHuI/QzuAm4BZtCsOTO/BPYFfgZMypzLcBV9r1ppWABIzzUDOLDzs+62IFbH2xt4MbATsE6PbazFsmJgs9X8uSeIguhqYkLk5dR/TsT1xLn5JTAtcy7D0aQtpCfkTkDqVu7HUIx6xY2svhPMbTxwKHAKcBv5z9etwNeJx/DqvBri5sSaDbnPV79x/eBPSTI3kv98dRsqXO4L0KhPXEs9l1odCxxBbDT0OPnP06riMeA/iFsnY5KcieHZiFi9Mfd56ieephlztkbRrCWaVbjcF6BRj7iR+nX+04h9Bh4g//npNR4gRinqtlTyRjR3JGC7BOdj0FwISI2S+wI08scdxBBxXexDrKm+hPznZhBxGXAk9ZnItglx6yL3eek13p3iZAzY+8h/nnoJFS73BWjkjbuJWfK5jQBeQ3OHqLuJa4jHKutQCGxFPOWQ+5z0Ev+T5EwM1rnkP0+9hAqX+wI08sVcYFfyOwz4HfnPR1VxNTFpMLcdiXkLuc9Ht1H37YA3Ih4nzX2eegkVLvcFaOSJBcCfkdd0mveNaZBxIfHYYk6HEKs95j4X3cZH0pyGgfgY+c9Pr6HC5b4AjTzxTvJZE/gqzep4UsUCYjvfNYZ1RofnXSvJq65xL/V83HINYqfM3Oen11Dhcl+ARvXxDfI5iNgsJfc5qFvcQWy2lMu3VpFXHeOjic7BcHyC/Oeln1Dhcl+ARrVxOXmeUR9PPNK3uMd8S4olwGnECEnVxgJX9phvrphLvZY2nkzsm5H7vPQTKlzuC9CoLh4gls6t2i40a3W03HE9edYP2JRY2jj36+8mLiYW3cltJLHMcu7z0W+ocLkvQKOaWEwMv1ftzTT321HOeAp4ex/ne7gOpjnrL3wu0TnoxUnkPw/DCRUu9wVoVBNfplqjiDXyc7/upsdXqH4J3Ca9b+9PdA66ccJq8mpKqHC5L0AjffyeamdOjwXOSPA6So0fEXMoqjIOuC7B60gRi4iOuGrv7hw79+sfbqhwuS9AI208Q7X3k9cGzk/wOkqPi6h2m9mdiYV3cr/ubuNzVDMnYBTNH/ZfPlS43BegkTY+RXU2IZa7zf2a2xq/Azbu+t0Yvs8meh2p4iJgyyRnIkwGLqnB6xxkqHC5L0AjXdxIDMdXYQrxLHvu19z2uL1zrqswDrgl8esZdDwBfJzB3jJZg3jOv42TWVW43BegkSYWA3tRjY1o5u5yTY07iUf2qrAPzXkqYPm4j1gwaDh7B2xELO/bxBX+ug0VLvcFaKSJ71CNCTjsnyOuA9br4v0ZhNMrek0pYgHwU+C9wPasfp7AKGAHYkvfc2nexj79RNHqsC1nbsVfBC30BLAtsfBPSmOJD8rcmwqV6mJiJ8X5iY+zOXAbMcGz6eYTS1E/QAzpQ7yuTYFtqO6WWV3YBxYudwVqDD4+TnojgTMzvT5jWfyYama//22m12ekjaJZ/XgRtM0sYpvd1N8Kvw58KPEx1J0vAScmPsZ4YgJiyln2ql7RfWDVK2xJqf096Tv/t2DnXycfA45KfIxngM8nPoZUqaKrnw5HANrjTmKi06KEx9iF2DUux451WrVHgN2AexIeYwzxtMfWCY+hahXdBzoCoDb5NGk7//HA97Dzr6OJwFmk3ep5ITHCJLWCBYDaYgbwg8TH+DKxRKzq6SWk76D/E7gr8TGkSlgAqC2+Siz+k8pBxLPUqrePA/slbH8xcGrC9qXKFH3/o8M5AM03h1infN7z/cE+rQlcD7wgUfsarJuBFxJD9imsRTxtMjFR+6pO0X2gIwBqg38mXecPMaxs598cO5D2KY0ngX9J2L5UiaKrnw5HAJptCTEre1ai9rcnlp1NOblMg/cUUQikui4mE3MBqliESOkU3Qc6AqCmO490H/IAX8POv4nWBL6SsP27gV8lbF9KzgJATZdyKPYw4NCE7Sut1wMHJmzf2wBqtKKHPzq8BdBcDxBLs6Z49n8EcBXw4gRtqzpXAHsnansssfDQxonaV3pF94GOAKjJziDdwj+vxs6/DV4GvDxR2wuAsxO1LSVnAaAmOzNh23+XsG1V65MJ2z4jYdtSUkUPf3R4C6CZ7gG2Is37dxBwYYJ2lc+ewNUJ2h1JTAjcIkHbSq/oPtARADXV2aQr3j6SqF3l8zeJ2l0C/DBR21JSFgBqqnMTtTsNOCRR28rnKNIt5vSzRO1KSVkAqImeBC5P1PZxFD4s2FIjgHckavvXpF2JUkrCAkBN9EtgfoJ2xwDHJGhX9fAO0izqNJ8oAqRGsQBQE52XqN1DgU0Tta38NiXdwkCprkkpGQsANVGqb1tHJ2pX9fHGRO1emqhdKRnvdfoYYNPMATYkZl8P0njgQWDCgNtVvTwGbEIs4jNII4FHgPUG3K7SKroPdARATXM5g+/8AfbHzr8E6wH7Jmh3CfDbBO1KyVgAqGmuTNTuYYnaVf2k2uAp1ZMpUhIWAGqa3yVq1wKgHKne62sTtSslUfT9jw7nADTLZsQugIO0BTB7wG2q3jYH7h9wm5OIJarVHEX3gY4AqEkeYvCdP8A+CdpUvb0sQZuziYmAUiNYAKhJrkvUborOQPW2d6J2b0zUrjRwFgBqktsStfviRO2qvvZM1G6qa1QaOAsANcnMBG2OBHZO0K7qbSfS3P+dkaBNKQkLADXJzARtbg2snaBd1du6wOQE7c5M0KaUhAWAmmRmgja3T9CmmmGHBG06AqDGsABQk6R4xGpqgjbVDFMStHl3gjalJCwA1BRLSfOI1VYJ2lQzpCj+Hsa1RdQQFgBqiseARQnaTXEfWM2QovhbCDyRoF1p4CwA1BSpFljZOFG7qr+NErXrYkBqBAsANcWjidqdmKhd1V+q994CQI1gAaCmeDpRuxskalf1l6oAeCZRu9JAWQCoKRYkanfNRO2q/lK99/MTtSsNlAWAmiLVh+rYRO2q/sYlatcCQI1gAaCmSDUCYAFQLgsAFc0CQJKkAlkAqClSfVNPNbKg+kv1TT3VyII0UBYAaopUH6oWAOWyAFDRLADUFKlGAJ5M1K7qL9V7bwGgRrAAUFOskajdVAsMqf5SLdgzPlG70kBZAKgpUi3a8nCidlV/qd77DRO1Kw2UBYCawgJAg5ZqBMDlpdUIFgBqinWBMQnadf/2cs1M0OZYYJ0E7UoDZwGgphhBmnX7ZyZoU80wM0GbDv+rMSwA1CRbJmhzRoI21Qwp3vsU16iUhAWAmmRKgjZvTtCmmuGmBG1OTdCmlIQFgJokxYfrDGBugnZVb48DsxO0awGgxrAAUJNMSdDmUuDGBO2q3m4g3vtBswBQY1gAqEm2S9Tu1YnaVX1dlajd6YnalQbOAkBNsmuidi9P1K7qK9V7vnOidqWBswBQk0wENk/QrgVAea5M0OZkYP0E7UpJWACoaXZJ0OZ9wO0J2lU93QQ8kKBdv/2rUSwA1DQvStTuLxK1q/pJ9V6nujalJCwA1DR7J2rXAqAc5yVqN9W1KSUxIncCNZDiUSCl8xgxF2DJgNsdBzxI7Dmg9noM2ARYMOB2RxJbS3v9NEvRfaAjAGqa9YDtE7Q7H/hJgnZVLz9i8J0/xNwUO381igWAmmi/RO2emahd1ccZidrdN1G7UjIWAGqiQxK1ez5wf6K2ld8DwMWJ2j40UbtSMhYAaqIDiXv2g7YIOD1Bu6qH7wALE7Q7nnSjUlIyFgBqorWAfRK1/a84MbSNlgL/lqjt/YA1E7UtJWMBoKY6MlG7d+IjgW10LnBXoraPSNSulFTRj0B0+G2vme4HJjH4xwEBDgB+laBd5bMv8JsE7Y4E7iHNEtVKr+g+0BEANdVmpFt45SLg2kRtq3pXkabzhxj+t/NXI1kAqMmOTtj2ZxO2rWqlfC/fmLBtKamihz86vAXQXA8CW5JmZjfEN8c9E7WtalwNvIQ0/87HAvcCGyZoW9Uoug90BEBNtgnpJgMC/F3CtlWNT5CuyH81dv5qMAsANd3xCds+H/h5wvaV1hnEfI5UjkvYtpRc0cMfHd4CaLYlwAuAmYna34bYP35sovaVxhPEnhH3Jmp/KvHIqF+imq3oPtCLV003EjghYft3AicnbF9pfJp0nT/ENefnpxqt6OqnwxGA5psDTAbmJWp/DeB6YjRA9XcTsBvpJoeuA9xN7EypZiu6D7SCVRusD7wjYftPE9/4LBbrbwnwl6Tr/CHmndj5q/EsANQWHwJGJWz/IuDUhO1rME4i3aI/AKOBDyRsX6qMBYDaYirw1sTHOBG4LvEx1L/fEvf+UzoG2CrxMaRKFH3/o8Nh3faYCUwHFiQ8xo7EAkFrJTyGevcwcd9/dsJjjAFuBbZOeAxVq+g+0BEAtckU4htaSjcBb8PCsU6WAu8kbecP8dy/nb9ao+jqp8MP8na5mxgFeCbxcb4CfCTxMdSdLwB/k/gYawC3EztQqj2K7gMdAVDbTAY+XMFxPk6sNKe8fgB8soLjfAw7f7VM0dVPhyMA7TOPGAW4L/FxxgDnAIckPo5W7iLglcD8xMfZArgN5320UdF9oCMAaqO1gX+o4DgLgTcA11RwLD3b74CjSN/5A3wJO3+1UNHVT4cjAO20BNgXuLyCY20IXEaMOii9PwJ7E9tBp7YfcDF+VrZV0e9r0S++wwKgvW4DXkj6CYEQTyBcAEyr4FgluwP4M2BWBccaB/ye2FRI7VR0H+gtALXZdOCvKzrWTOClxGI0SuN3wD5U0/kD/B12/mqxoqufDkcA2m0BsAdwQ0XHWxs4GycGDtpFwGuAuRUdb1fgamKip9qr6D7QEQC13Vjg+8Rz3FWYB7yKeDxNg/Ej4HCq6/zHAadj56+WswBQCXYEPl/h8RYQ+xJ8CUeYhmMpscjPG6hmHseQLwO7VHg8SZksNYqIJcQz41U7CHigj3xLj4fI834dQlwruV+/UU0Urej7Hx3FXwQF+ROwO+nXjF/RJOI2xD4VH7ep/hd4IzGxskpbEms6bFTxcZVP0X2gtwBUko2JCXrjKj7ubOAVwGeIb5dauaXAqUShNLPiY48Dfoidv1SU3ENQRvXxTfLZD7hxFXmVHNcDLx/GeR2ub68iL6PdocLlvgCNPHE8+YwGPkjMas99HnLHk8Cniac1cnk3+c+DkSdUuNwXoJEnFgFHkNfmxONmuc9FrjiH2L0xp0OJPR1ynwsjT6hwuS9AI1/MJRZ8ye1A4FLyn4+q4krggIGcueHZCXiM/OfDyBcqXO4L0Mgb9wBbUQ/7AueR/5ykiqvI82jfykwlJmfmPidG3lDhcl+ARv64kxiOr4tdiVsDi8h/boYbS4ALgSMHeoaGZwtiR8Hc58bIH0Ur+hnIjuIvAgFwO/ENvIotZru1JfAW4D3kv1feq/uJIuZfiM62LjYEfg3skDsR1ULRfWDRL77DAkBD/gAcTKxCVyejiRUF3wi8GlgvbzqrNAf4H+AM4FfECEadbESMRtRh3ofqoeg+sOgX32EBoOXdSnS29+ZOZBXGEmsJHNqJ3N9kbyLmLZxHTGRckDedVdqU6Px3yp2IaqXoPrDoF99hAaAVzSSKgDoNXa/KpsDendiT6ODWTXSsx4ltla8GLgMup163TFZlK+CXwDa5E1HtFN0HFv3iOywAtDKziS1or8+dSB+mANsTM92nEPMHNgYmdmJN4rbCOp0//wQxXP8U8EgnHiSekJjRiVuAWRXlP0gvBM4lJv5JKyq6Dyz6xXdYAGhV5gFvAn6WOxH15WDgLGBC7kRUW0X3gW4GJK3a2sSktnflTkQ9ezdRuNn5S6tgASCt3mjgtE7kXK9e3RkHnAJ8i3jvJK1C0cMfHd4CULeuAV5HM++Fl2ASMeS/V+5E1BhF94GOAEjd251Yzvbg3InoOQ4DrsXOX+qaBYDUm02IZ95PI2bTK6/xwEnETP+NMuciNUrRwx8d3gJQv64H/px4Nl7V2wX4L1zcR/0rug90BEDq3y7EvICTiMlnqsYY4ERiQSI7f6lPRVc/HY4AaBBuBY4nVshTOvsSGwxtmzsRtULRfaAjANJgbEeshX8msfSsBmtzYnfBS7Dzlwai6OqnwxEADdpTwJeBLwJPZ86l6dYAPgD8LcuWLpYGpeg+sOgX32EBoFRmA18B/hmYnzmXphkDvBn4NLGngZRC0X1g0S++wwJAqd0FfJaYsb4ocy51Nxo4Bvh/xEZGUkpF94FFv/gOCwBVZSYxGnAa8FjeVGpnbeCdwIdxDoWqU3QfWPSL77AAUNXmsGx/gZl5U8luKnBCJ9bLnIvKU3QfWPSL77AAUC5LgIuAbwM/ARbkTacyY4FDgLcBrwVG5U1HBSu6Dyz6xXdYAKgOHiI2sjmDWEtgSd50Bm4k8Qz/G4HXAxvmTUcCCu8Di37xHRYAqpv7gLOJ/ewvBZ7Jm07fxgP7AUcQuyhuljcd6TmK7gOLfvEdFgCqs6eAi4kNiH4N3ER9RwdGATsC+wOHEp2/GyapzoruA4t+8R0WAGqSx4ErOnENsRHR7Ey5bAnsTGyT/LJOTMiUi9SPovvAol98hwWAmm4OcB1wOzCjEzOBu4FH6H9y4VhgIjCZmK0/pfNzOrER0vrDyFmqg6L7wKJffIcFgNruCeBh4FFgHrAQWAzM7fz/CcTw/RjiefyJnXDpXbVd0X1g0S++wwJAkspUdB/oboCSJBXIAkCSpAJZAEiSVCALAEmSClR6AVD0BBBJKlzRfWCpL34csef49bkTkSRlcwfwQWCt3InkUNo34I2BvwA+AGyeORdJUj3MBb4LfJVYQKsIpRQA2wLvBY4H1siciySpnhYDvwD+Abgycy7Jtb0A2Ac4ETic9r9WSdLgXAOcCvw3sChzLkm0sVMcAbwG+BSxXrkkSf26Dfgc8H1ihKA12lQAjCD2Hf8UsTuZJEmDMgM4Cfg3WjIi0JYC4CDijbHjlySl1JpCoOkFwEHAF4A9ciciSSpK4wuBphYABwBfBl6UOxFJUtFuBT4B/CR3Ir1qWgEwDfg88IbciUiStJyLgY8Av8+dSLeaUgCsTzzO9yFiFT9JkupmCfBfRH91f+Zcnteo3Ak8j9HAccCPgYM7v5ckqY5GALsC7wYmAL8FFmTNaDXqPAJwKHAyMD13IpIk9eFu4GPAmbkTWZk6FgDrEzMr35U7EUmSBuDnwF9Ss30G6nYL4A3EOsz75k5EkqQBmQacQKwk+Ftgad50Ql1GALYA/gk4KncikiQldCWxMd1NuRPJPQIwitiL+cfAzplzkSQptS2Jye1jgCvIuL9AzhGATYDTidn9kiSV5lrgzcDtOQ6eawTgKOB8/NYvSSrXZsRowDzgqqoPXvUIwHjgi8D7MxxbkqS6Opt4+m1OVQesshPejthP+YUVHlOSpKaYBbwVuLyKg1V1C+DNpJ58XAAAF/ZJREFUwM+IyQ+SJOm51gPeBjwKXJ36YKkLgBHEmsjfBMYmPpYkSU03CnglsDkxV25JqgOlvAWwFjHL/7UJjyFJUlv9Bngd8FCKxlMVAFsQeyPvnqh9SZJK8EfgVcDNg2545KAbBPYErsHOX5Kk4XoBMSnwgEE3POgC4OXAhcQiP5IkafjWIzYUevUgGx3kJMBXAOcC6wywTUmSBKOB1wMzgOsH0eCgCoAjiHv+aw6oPUmS9GwjiZV0ZwO/H25jgygA3gycgY/5SZKU2kjgSOLJgN8Np6HhFgBHEJ3/6GG2I0mSujOCWCtgFvCH4TTSr5cAvyKe95ckSdVaSDwieF4/f7nfAmAHYoGCDfr8+5IkafieAg4EftvrX+ynANgcuALYqo+/K0mSButhYG/g9l7+Uq8FwBpE5++OfpIk1cdtwIuBJ7r9C70uBPRP2PlLklQ304F/7eUv9PIUwHHA3/WUjiRJqsqOwBzgqm7+cLe3AHYhJhis0WdSkiQpvYXEyryXP98f7KYAWIt4znCbYSYlSZLSm0V8cZ+7uj/UzS2ALwKHDyIjSZKU3HrABGIDoVV6vhGAFwNXMthNgyRJUlpLgP2Ay1b1B1ZXAIwh1hneZcBJNdFS4mRaCElSvS0mnnAbzkq3bXELsBswf2X/c3WPAX4EO/+lwE+JkZC3E0WAJKmelgDHEI+r/4AoBkq2PfCJXv/SROBxogMsNS4kOv7lvacGeRmGYRgrjw/xbFOBU4Cna5BbrngC2ISVWNWQ9t8TjxGUZinwM+CtxOTH+1b4/1d3/kyJ50aS6uyTwJdX+G+PERvlnE480bYrvS+A13RjiR17z+/mD08iNhfIXbVUGYuBs+n+lsdnapCzYRiGEcP+3S5Sty3wfeIzP3feVcYzwORuTtA/1yDZKuM3wIu6OTEreAewoAb5G4ZhlBoLgGPp3YuAS2uQf5Xx7RVPwoqzJNcHZgNrdnsWG+xe4G+A/yROTj8OBH4IrDuopCRJXZkHHA38YhhtHAmcCkwZREI1Nx/YEnho6D+seC/kHbS/83+amOOwLXFfqN/OH+BXwD7AjQPIS5LUnRuAvRhe5w9wDrF+/meJvqHNxrGa0ZIRxHaCuYcpUsY5xKzQQRsPnER595UMwzCqjCXAaaT5ojqJ+FK4pAavM1XMZBWT/w+uQXKpYjZw6Mpe9IAd3DlW7tdrGIbRtrgHOIj0DqPdn+P/1xcufwvgjcM4YXV2FjG7/7wKjnUBsSfzJ4hnLyVJw/MU8Vj2jsAvKzjeLzrHes6kuZZ4w4r/YRTwJ/JXJoOMB4HXDu88DcvmxFDVIvKfC8MwjKbFYuBMYCvyeSWxHkzuczHIeJhYF+D/7FuDpAYZPwQ2oh62B/6J2JYx93kxDMOoe8wFvkF8dtbBxsCPyX9eBhn7Lv8Cv1qDhAYRi4CPU89NICYA7wNuJv95MgzDqFvcBLwXWIf6GUHc2m3LiO6Xh14UwFXAnoM5T9nMBd5GbN5Td1sTk1mOJCYOjs2bjiRVbjHwW+LprF8C1+RNpyuHECsJrp87kWG6EnjZCOLZwMc7P5vqZuDVwB25E+nDesDewE7Azp2f22NRIKk9FhCf0zcRz/DfAFxO9D1Nsw1xS2Cn3IkMw3xgwghiMYUrMyczHJcTEzXm5k5kgEYDGxArDE4gqs11KW8TC0nNs4To2Od0fs4FHiWGz9tiHeBcVriX3jB7QbO3uL2I2OFJkqQqrUnssJe7H+w3/nIkcT+6ic4DDgeezJ2IJKk4TwGvAn6SO5E+TR1JmqVxU7sWeB3tX7tZklRf84mFdS7KnUgfpkDMvMw9FNFL3EsssiNJUh1MBG4nf//YS1wNMKsGiXQbi4CXdvmGSJJUle2I2wK5+8luY8ZImjWJ7ps0+4kFSVI73Qp8OncSPVhrBFGxrJE7ky48Qwz9z8mdiCRJKzEGuBvYNHciXXhqJM1ZcOZa7PwlSfW1ELg/dxJdGjcSmJc7iy7tRDNGKiRJZdoL2C13El2aN5LmrKA3ATgudxKSJK3EWOBbuZPowRMQazPnno3YbTwGbJHiTEiSNAxfIn8f2Uvc1KQRAIj18E8n1sqXJKkOjgY+kjuJHj02EpidO4seHUA8Djji+f6gJEmJvRL4T5q3WdsfRxJbNDbN8cC3ad4JlyS1x37A2TTnabrl3drUAgBiQuB/4e0ASVL1Xg38nOY+nXYbwM7kn4wwnDgLGD/gEyNJ0qp8EFhM/v5vOLEDwDhilb3cyQwnrqW52xpLkpphJPB18vd5w42HWG4e3a9qkNBw43Hgtat40yRJGo6NgfPI39cNIs5c/oV9vAYJDSKWACcBo57z1kmS1J8DgPvI38cNKv5y+Re3aw0SGmRcCkxDkqT+jQb+gebf718xtoVl9wBGENVNE3Yw6tbTxNaMXwMW5U1FBVsXOJhYI3wTYs7NA8QM3J8BM/KlJmk1dgC+Q/zbbZPfAy9a8T+eSv6qJEVcDewynLMl9WEL4J+B+az++rwE2DtPipJWYizwKZo/OX5V8Vcre9Ftuw2wfCwAPkt8+5JSew2x0UYv1+jJuKaFlNtewI3k77NSxSJgs1W9+KtrkGDKuItYs9llhJXKR4nJqP1cn+cDa1afslS8dYBTaN+9/hXjF6s7Ce+uQYJVxP8C+6zuREh9+CjDvzYvBdauOnGpUCOBY2jXDP/VxUGrOxkTgDk1SLKKWAJ8H5iyuhMidekTDO7avAhHAqTUDgFuIH9fVFVc081J+VQNEq0ynibWDli/m5MjrcQgO/+hsAiQ0tiRGArP3fdUHUd3c3LWBR6tQbJVx+PA54ENuzlJUkeKzn8oLAKkwZkEnEZMhMvd31Qdt9LDAnn/rwYJ54p5xGSQzbs9WSrWIO75P184J0AansnEZ/rT5O9fcsXhvZywCcRmAbmTzhlPEY9mbdHLiVMxUn7zXzEcCZB6tzXwLzz/Whxtj/P7OXl/UYPE6xDPAN/E3Qa1TJWd/1BYBEjdmQb8O7CQ/P1H7lhIZ9vfXo0gVinL/QLqEouBnxCPUbiOQLlydP5DYREgrdoewPco8x7/quJrwzmh29Le5RCHE7cDH8R7s6Wp4p7/84VzAqRlRgFHAheS/99m3eIWYI3+T234bA1eSF1jDvBV4AV9n101Rc5v/iuGIwEq3cbAJ4F7yf/vsY6xEHhJ32d3OWOJlfNyv6A6x2Lgp8TCEiP7O82qsTp1/kNhEaAS7QH8B45MP1/8fb8neGWmUs4KgcONe4j9o6f3daZVN3Xs/IfCIkAlmAC8E7iC/P/mmhBXAGP6OtOr8boavLCmxe+IuQIuLtRMdbjn/3zhnAC10Uhiv5bT6H1nzZLjARI+uv6NGrzAJsYzwDnAG3DL16ZoQuc/FBYBaovJwInEzq25/101LRYA+/Z+yrs3Dh8NHG7MJvYe2KW3U68K/RX5r5New9sBaqoJwHHAZfS/lbYB7+31xPdjfeDmDC+ujXE78AViYovq4VU0d09wiwA1xQTgzcDZxKqruf/tND1O7u30D89U4l5D7hfdpphBPFL4MnySIJcNgcfIfy0MJywCVFcbAMcSt0OdxT+4+B4Z+ozdiY1zcr/4NsafgNOJBS4GPptTq/RV8r/3gwjnBKguJgLHEJ1+6Wvyp4gLiUf1sziEsndYqiIeJGbCHgms1d3boj6MBeaS//0eVDgSoFwmAx8Afo3L8qaMy6jBv/GDsQioKhYSb/qJxAiMexIMzsHkf38HHY4EqAprEHuknEQ8+uxEvmr+ba/TzZtTBYuAPPEgcCYxxLbB875LWp2/Jv/7mSIsApTC1sC7iM+fNo2cNSEuoYb/pi0C8sYiovo+iajGnTvQm5PJ/x6mCosADdfaxG3I04CZ5L+mS42fM4ANflLZH5cMrks8DPwQ+BBxu2DUqt82AaeS/z1LGc4JUC/WAf4M+AxwOd7Lr0N8n4wT/rq1IzCL/CfLeHbMI+YPDI0Q1LaKzORvyP8epQ5HArQqmxDf8E8iPiecsV+vOIUGPR6+BXAd+U+aseqYT/xD/wJwOLDeSt/JchxJ/vekinAkQCOA7YnV974L3En+69JYeSzovE9JpJxFPoEYgj4o4TE0OEuAG4DfEEN+VwN/zJpRtdYEHgHG506kAhcDRxArsKn91gB2IxYZ2wfYGzcoa4LHgaOBC1IdIPVjZKOBLwEfTnwcpfEoUQgsH/dnzSitfyW2HS2BRUA7jSX2GdljudgRNyFrmhuJHXhvT3mQqp4jfxPx4epCNs13L3AN8IflYkbWjAZnMnAbZYwCgEVA061DdPa7duJFnd/XfqKYVusHxLD/k6kPVOVCMjsBPwKmVXhMVeMxnl0Q3ADcQjwW2jTHA9/OnUSFLALqbwQwhfgMHersdyOexXcxsPZYCHyMmPBXiaovnnWBfwdeU/FxVb0lxMjAjcTukUNFwS3EBMQ6Oxn4YO4kKmQRUA8jiY3WdlgudgS2w9HTtpsB/DlwRZUHzVU9Hg98HS/qEi0iLvbbOnE7cEfn1/dlzGt5I4BvAu/OnUiFLAKqswGw7XIxrRPb4eO5JTodeD+xomKlcg4fTSO2MdwzYw6qlyeIguB24gmEGcBdnZ+zgcUV5mIRoH6NADYjvs1vvdzP6cTn3sR8qalG5hCfL2fmSiD3/aPRwKeIddhdqU6rswC4m2UFwYzO7+/p/LyfuIc2SBYBWplRwKbAVsAkYvLo1OViCuVMJFV/LgTeQXyxySZ3ATDkpcRTAjvkTkSNtZjYHGkW8Y9qdufX93fiT8QTDPN6bNcioCxjiFXxtiQ6+S2JDn4Syzr7zfCxOvVnDvARYgGmpXlTqU8BAPHoyieIJVnHZc5F7fUUMdfgAaJguI8oDh7u/Hyk8+uhn4uwCGiTqcQqeJsSHfpmxMqlWwCbE51/nT4X1R5nAR8gPntqoY4X+nbEY1gvz52IRDzi+BBREOxBWd/82lIE7EFMsjoc77+revcC7wV+kjuRphhB7DX9KPnXYjaMkqPJewdsQOyglvscGmXGfOCLxIJNtVTHEYDlbQj8PfHYoJMEpTwupnkjAdsQeU/KnYiK9HNiCfykS/kOV90LgCG7EOsGHJA7EalQTSoCtiQ2tNoydyIqzp1Ex39u7kS60ZT9ha8HDgReSzwGJqlaryA+1Op+O2AE8B3s/FWtR4C/IpZrbkTn31TjiBP9EPnv8RhGaVH3OQFvJv85MsqJJ4HPE8vcN05TbgGszFrA+4hHB9fLnItUkt8Ar6T3NRWq8FvgJbmTUOstAf6L6H/qsoR5z5pcAAyZSLwJ78V1tKWqXEz95gRsTSwhLaWymHiy5HPE/iWN1pQ5AKvzCLGF4jbAPwHP5E1HKsIrgB8SK+fVhfuKKJVFxOp92wNvowWdP7SjABhyH3FLYCrwZWJjGUnpHEos4V0X03MnoNZZQFzj04G/IHYubY02FQBDHgA+TmzI8SlihEBSGscQi3bVQRtuaaoe5gEnE7s3Hk9Lnz5rYwEw5FHgs0Qh8FEy77oktdjQB2Vug94NUuW5n9iddkvief6786aTVpsLgCHzgK8SW3e+Cvhl3nSk1lkD+FruJIjNnaR+3AF8iJhIehKxB0jrlVAADFkCnAP8GfGY0H/jNwZpUI4A9s+cw72Zj69mWUxs0HMIcY//FAqbRF76PbPNgfcQ93g2zpyL1HTnAYdlPP6uwB8yHl/N8BAxse80YFbmXLIqvQAYMhZ4NVEIHIjnRerHUmIuQK5n8TckPtyllfkt8aj4WcROfcWzo3uuScBbiYWFXE9c6s0niaVRcxhBLEw0PtPxVT+PAWcC38LRoeewAFi1McBRwHHAQbgdsdSNq4C9Mh7/LmItEJVrMXAB8G/AT4ln+bUSFgDd2ZzYZOQYYmtiSSu3ENiAfPsEXAbsnenYyus24HvEin0+9t0FC4De7QAcDbydWGNA0rO9DLgy07HPIP59qgz3AWcT9/UvJ+ahqEslPQY4KDcDnyb2HjiYqDbnZMxHqpttMx67sTuzqWuPE8P7BwGTgQ8SIz92/j0anTuBBlsMXNiJscTTA28g5g1skDEvKbfNMx7bAqCdHiPWcfkhcD6FPa+figXAYCwAftGJUcBLiWLgTbi+gMqzdsZjuxhQe8wBziWG9y/AR/cGzgJg8BYTw1GXEXsQ7A8c2Ykp2bKSqpPziRlHAJptNtHp/xC4hNiGV4lYAKS1kGW3CT5ArDN9JDE68FKcg6F2yrkVtyMAzXMzMbx/Lk7kq5RPAeSzKbF++uHE/IF18qYjDcwJwLczHXst8j2CqO48SWzKdm4nHsibTrksAOphFPBCYlbrkTg6oGY7ALg44/HnAOtlPL6e6y6isz+HuD3qJL4asACop42IYuAQ4lHDzfKmI3VtKbAJedfkv4lYr0P5PEjc+jy/89OtmmvIAqD+RhAfZvt3Yj+iQJDq6GZgx8w5XEBs+63qPAr8hhj5uQS4Hu/l156TAOtvKfGN5iZiJ6uhguAVRDFgQaA6OT93AvgkQBWGOvxLWNbhL8mYj/pgAdA8yxcE3yAKgh2BlxNLsL6MeNpAyuEHuRPAJwFSuJuYoX8FcQ/fDr8FvAXQTpsSEwn37vzcHRiXNSOV4DpgN/IP/b6XKI7Vn0XA74nOfijcXKeFHAFopweAH3cCovPfndimdY/Or6dhAajB+hL5O3+ws+rVTOB/gas78TviUT21nB1AudYBdiWKgaHYHq8J9ecm4lHWOqzctjXwx9xJ1NRjRAd/OXANcBXwp6wZKRs/7LW8iUQh8EJgF6JAmA6MyZmUam8p8YTKpZnzGDKCeAxxYu5EMlpKFEF/6MR1nZ+Ojuj/WADo+YwlJhnuslzsik8eaJmTgQ/nTmIF/wi8L3cSFZkH3MKzO/rrybsksxrAAkD92pR4HHE7okDYjriF4KJFZbkK2JfYEbNOphO3JXJuTDRo84BbgRuJ9RZu6vycRT3mXqhhLAA0aOuxrCiYThQF04CpxGiC2uNO4kmTut5DPgk4MXcSfZgN3A7c0fl5K/ENfyZ29BogCwBVZRQwGdhmhZhGTNryMcVmuZu47z8jcx6rM45Yme6luRNZiQeIc3cncBvLOvs7cAa+KmIBoDoYCWwJTAG26vyc3Pn1Vp1fWyDUx93ESpR35U6kCxOAnwH7VHzcuUQHv7K4C3i64nyk57AAUBOMIOYcDBUEWwJbAJt3Ygti7sH4XAkWpEmd/5CxwN8Cf81gnmh5hhimn02cj3uW+/2szs85AziOlJQFgNpkQ6IQmEQUDFsSTytsAmzc+fVGnT/ntd+7Jnb+y5sKvBt4M3FtrMyfiOH5ezo/ZxN7C9xHLDF8H+5sp5bwQ1AlGsWyQmBjoljYkHhufP1ObLDcr4d+X/J6CE3v/Fe0MfEUy2hiOP5e4H5gfs6kJEn1tDbxzfFrxGzsUmIWbjAlSSrcscBi8nfKVcXdwAsGceIkSWqqY7HzlySpKMdi5y9JUlGOxc5fkqSiHIudvyRJRTkWO39JkopyLHb+kiQV5Vjs/CVJKsqx2PlLklSUY7HzlySpKMdi5y9JUlGOxc5fkqSiHE1Znb8b+0iSircb8CT5O2W/+UuSVJFRwI3k75Tt/CVJqtAx5O+UqwqH/SVJ6vg1+Ttmv/lLklSh9YCF5O+c/eYvKZuRuROQMtgOGJ07icTuAQ4A7sqdiKR6sgBQiTbOnUBidwP7A3/MnIekGrMAUIkW5k4gIb/5S+qKBYBK9EDuBBLxm78kSasxDphL/kl6zvaXJKliPyZ/p+1sf0mSKrYv+Ttuv/lLkpTBeeTvwP3mL0lSxTYlvkHn7sj95i9JUsV2Bx4jf4fuN39Jkiq2O/Ao+Tt2v/lLklSxJhQBdv6SJCVQ5yLAzl+SpITqWATY+UuSVIE6FQF2/pIkVagORYCdvyRJGeQsAuz8JUnKKEcRYOcvSVINVFkE2PlLklQjVRQBdv6SJNVQyiLAzl+SpBpLUQTY+UuS1ACDLALs/CVJapBBFAF2/pIkNdBewEP01/nfgVv6SpLUWFOBP9Bb538esH6OZCVJ0uCMAU4AZrH6jv8G4LXAiDxpStIyfhBJgzOCmBtwADAJ2IC4RTADuAC4NV9qkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJUkv9fx9Hvqt7fu1LAAAAAElFTkSuQmCC"

/***/ }),

/***/ "jimu-core":
/*!****************************!*\
  !*** external "jimu-core" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_jimu_core__;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		__webpack_require__.p = "";
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/nonce */
/******/ 	(() => {
/******/ 		__webpack_require__.nc = undefined;
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other entry modules.
(() => {
/*!******************************************!*\
  !*** ./jimu-core/lib/set-public-path.ts ***!
  \******************************************/
/**
 * Webpack will replace __webpack_public_path__ with __webpack_require__.p to set the public path dynamically.
 * The reason why we can't set the publicPath in webpack config is: we change the publicPath when download.
 * */
// eslint-disable-next-line
// @ts-ignore
__webpack_require__.p = window.jimuConfig.baseUrl;

})();

// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!***************************************************************************!*\
  !*** ./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.tsx ***!
  \***************************************************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var jimu_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jimu-core */ "jimu-core");
/* harmony import */ var _camera_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./camera.png */ "./your-extensions/widgets/cameraViewRenderer/src/runtime/camera.png");
/* harmony import */ var _camera_png__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_camera_png__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _widget_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./widget.scss */ "./your-extensions/widgets/cameraViewRenderer/src/runtime/widget.scss");



const Widget = (props) => {
    const showCameraView = () => {
        console.log(props.data.attributes); //geometry
        const sr = jimu_core__WEBPACK_IMPORTED_MODULE_0__.Immutable.asMutable(props.data.geometry.spatialReference);
        props.widgetProps.dispatch(jimu_core__WEBPACK_IMPORTED_MODULE_0__.appActions.widgetStatePropChange('widget_237', //'widget_235', //'widget_232', //id of camera-view widget .. should be changed if the widget removed and re-added using exp builder
        'selectedCameraViewGraphic', {
            attributes: props.data.attributes,
            geometry: {
                x: props.data.geometry.x,
                y: props.data.geometry.y,
                z: props.data.geometry.z,
                spatialReference: sr
            }
        }));
    };
    return (jimu_core__WEBPACK_IMPORTED_MODULE_0__.React.createElement("button", { className: 'camera-view-renderer_btn', onClick: showCameraView },
        jimu_core__WEBPACK_IMPORTED_MODULE_0__.React.createElement("span", null,
            jimu_core__WEBPACK_IMPORTED_MODULE_0__.React.createElement("img", { className: 'camera-view-renderer_icon', src: (_camera_png__WEBPACK_IMPORTED_MODULE_1___default()), title: "Camera view" }))));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Widget);

})();

/******/ 	return __webpack_exports__;
/******/ })()

			);
		}
	};
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0cy9jYW1lcmFWaWV3UmVuZGVyZXIvZGlzdC9ydW50aW1lL3dpZGdldC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDc0g7QUFDakI7QUFDckcsOEJBQThCLG1GQUEyQixDQUFDLDRGQUFxQztBQUMvRjtBQUNBLHFFQUFxRSxpQkFBaUIsa0NBQWtDLGdCQUFnQixlQUFlLEdBQUcsOEJBQThCLGdCQUFnQixpQkFBaUIsR0FBRyxPQUFPLDBTQUEwUyxVQUFVLFdBQVcsVUFBVSxVQUFVLEtBQUssS0FBSyxVQUFVLFVBQVUsK0NBQStDLGNBQWMseUJBQXlCLDBDQUEwQyx3QkFBd0IsdUJBQXVCLFNBQVMsZUFBZSx3QkFBd0IseUJBQXlCLFNBQVMsS0FBSyw4QkFBOEIsaUJBQWlCLGtDQUFrQyxnQkFBZ0IsZUFBZSxHQUFHLDhCQUE4QixnQkFBZ0IsaUJBQWlCLEdBQUcsbUJBQW1CO0FBQ2xpQztBQUNBLGlFQUFlLHVCQUF1QixFQUFDOzs7Ozs7Ozs7Ozs7QUNQMUI7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRDtBQUNyRDtBQUNBO0FBQ0EsZ0RBQWdEO0FBQ2hEO0FBQ0E7QUFDQSxxRkFBcUY7QUFDckY7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLGlCQUFpQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIscUJBQXFCO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWLHNGQUFzRixxQkFBcUI7QUFDM0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWLGlEQUFpRCxxQkFBcUI7QUFDdEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWLHNEQUFzRCxxQkFBcUI7QUFDM0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNwRmE7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVEQUF1RCxjQUFjO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQkEsTUFBMkc7QUFDM0csTUFBaUc7QUFDakcsTUFBd0c7QUFDeEcsTUFBMkg7QUFDM0gsTUFBb0g7QUFDcEgsTUFBb0g7QUFDcEgsTUFBaVQ7QUFDalQ7QUFDQTs7QUFFQTs7QUFFQSw0QkFBNEIscUdBQW1CO0FBQy9DLHdCQUF3QixrSEFBYTs7QUFFckMsdUJBQXVCLHVHQUFhO0FBQ3BDO0FBQ0EsaUJBQWlCLCtGQUFNO0FBQ3ZCLDZCQUE2QixzR0FBa0I7O0FBRS9DLGFBQWEsMEdBQUcsQ0FBQyw4T0FBTzs7OztBQUkyUDtBQUNuUixPQUFPLGlFQUFlLDhPQUFPLElBQUkscVBBQWMsR0FBRyxxUEFBYyxZQUFZLEVBQUM7Ozs7Ozs7Ozs7OztBQzFCaEU7O0FBRWI7O0FBRUE7QUFDQTs7QUFFQSxrQkFBa0Isd0JBQXdCO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsa0JBQWtCLGlCQUFpQjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsb0JBQW9CLDRCQUE0QjtBQUNoRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxxQkFBcUIsNkJBQTZCO0FBQ2xEOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUN2R2E7O0FBRWI7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esc0RBQXNEOztBQUV0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOzs7Ozs7Ozs7OztBQ3RDYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7QUNWYTs7QUFFYjtBQUNBO0FBQ0EsY0FBYyxLQUF3QyxHQUFHLHNCQUFpQixHQUFHLENBQUk7O0FBRWpGO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7OztBQ1hhOztBQUViO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGtEQUFrRDtBQUNsRDs7QUFFQTtBQUNBLDBDQUEwQztBQUMxQzs7QUFFQTs7QUFFQTtBQUNBLGlGQUFpRjtBQUNqRjs7QUFFQTs7QUFFQTtBQUNBLGFBQWE7QUFDYjs7QUFFQTtBQUNBLGFBQWE7QUFDYjs7QUFFQTtBQUNBLGFBQWE7QUFDYjs7QUFFQTs7QUFFQTtBQUNBLHlEQUF5RDtBQUN6RCxJQUFJOztBQUVKOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7O0FDckVhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7QUNmQSxpQ0FBaUM7Ozs7Ozs7Ozs7O0FDQWpDOzs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7OztXQ3RCQTtXQUNBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsaUNBQWlDLFdBQVc7V0FDNUM7V0FDQTs7Ozs7V0NQQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHlDQUF5Qyx3Q0FBd0M7V0FDakY7V0FDQTtXQUNBOzs7OztXQ1BBOzs7OztXQ0FBO1dBQ0E7V0FDQTtXQUNBLHVEQUF1RCxpQkFBaUI7V0FDeEU7V0FDQSxnREFBZ0QsYUFBYTtXQUM3RDs7Ozs7V0NOQTs7Ozs7V0NBQTs7Ozs7Ozs7OztBQ0FBOzs7S0FHSztBQUNMLDJCQUEyQjtBQUMzQixhQUFhO0FBQ2IscUJBQXVCLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOcUI7QUFHbkM7QUFDZjtBQUV0QixNQUFNLE1BQU0sR0FBRyxDQUFDLEtBQTBCLEVBQUUsRUFBRTtJQUM1QyxNQUFNLGNBQWMsR0FBRyxHQUFHLEVBQUU7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFDLFVBQVU7UUFFN0MsTUFBTSxFQUFFLEdBQUcsMERBQW1CLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUM7UUFFcEUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQ3hCLHVFQUFnQyxDQUM3QixZQUFZLEVBQUUsb0lBQW9JO1FBQ25KLDJCQUEyQixFQUMzQjtZQUNFLFVBQVUsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVU7WUFDakMsUUFBUSxFQUFFO2dCQUNSLENBQUMsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLGdCQUFnQixFQUFFLEVBQUU7YUFDckI7U0FDRixDQUFDLENBQ0w7SUFDSCxDQUFDO0lBRUQsT0FBTyxDQUNMLHVFQUFRLFNBQVMsRUFBQywwQkFBMEIsRUFBQyxPQUFPLEVBQUUsY0FBYztRQUFFO1lBQ3BFLG9FQUFLLFNBQVMsRUFBQywyQkFBMkIsRUFBQyxHQUFHLEVBQUUsb0RBQVUsRUFBRSxLQUFLLEVBQUMsYUFBYSxHQUFHLENBQzNFLENBQVMsQ0FDbkI7QUFDSCxDQUFDO0FBRUQsaUVBQWUsTUFBTSIsInNvdXJjZXMiOlsid2VicGFjazovL2V4Yi1jbGllbnQvLi95b3VyLWV4dGVuc2lvbnMvd2lkZ2V0cy9jYW1lcmFWaWV3UmVuZGVyZXIvc3JjL3J1bnRpbWUvd2lkZ2V0LnNjc3MiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC8uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanMiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC8uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9zb3VyY2VNYXBzLmpzIiwid2VicGFjazovL2V4Yi1jbGllbnQvLi95b3VyLWV4dGVuc2lvbnMvd2lkZ2V0cy9jYW1lcmFWaWV3UmVuZGVyZXIvc3JjL3J1bnRpbWUvd2lkZ2V0LnNjc3M/ZDNmNCIsIndlYnBhY2s6Ly9leGItY2xpZW50Ly4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzIiwid2VicGFjazovL2V4Yi1jbGllbnQvLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbnNlcnRCeVNlbGVjdG9yLmpzIiwid2VicGFjazovL2V4Yi1jbGllbnQvLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbnNlcnRTdHlsZUVsZW1lbnQuanMiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC8uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL3NldEF0dHJpYnV0ZXNXaXRob3V0QXR0cmlidXRlcy5qcyIsIndlYnBhY2s6Ly9leGItY2xpZW50Ly4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvc3R5bGVEb21BUEkuanMiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC8uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL3N0eWxlVGFnVHJhbnNmb3JtLmpzIiwid2VicGFjazovL2V4Yi1jbGllbnQvLi95b3VyLWV4dGVuc2lvbnMvd2lkZ2V0cy9jYW1lcmFWaWV3UmVuZGVyZXIvc3JjL3J1bnRpbWUvY2FtZXJhLnBuZyIsIndlYnBhY2s6Ly9leGItY2xpZW50L2V4dGVybmFsIHN5c3RlbSBcImppbXUtY29yZVwiIiwid2VicGFjazovL2V4Yi1jbGllbnQvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC93ZWJwYWNrL3J1bnRpbWUvY29tcGF0IGdldCBkZWZhdWx0IGV4cG9ydCIsIndlYnBhY2s6Ly9leGItY2xpZW50L3dlYnBhY2svcnVudGltZS9kZWZpbmUgcHJvcGVydHkgZ2V0dGVycyIsIndlYnBhY2s6Ly9leGItY2xpZW50L3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovL2V4Yi1jbGllbnQvd2VicGFjay9ydW50aW1lL3B1YmxpY1BhdGgiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC93ZWJwYWNrL3J1bnRpbWUvbm9uY2UiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC8uL2ppbXUtY29yZS9saWIvc2V0LXB1YmxpYy1wYXRoLnRzIiwid2VicGFjazovL2V4Yi1jbGllbnQvLi95b3VyLWV4dGVuc2lvbnMvd2lkZ2V0cy9jYW1lcmFWaWV3UmVuZGVyZXIvc3JjL3J1bnRpbWUvd2lkZ2V0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBJbXBvcnRzXG5pbXBvcnQgX19fQ1NTX0xPQURFUl9BUElfU09VUkNFTUFQX0lNUE9SVF9fXyBmcm9tIFwiLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL3NvdXJjZU1hcHMuanNcIjtcbmltcG9ydCBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIjtcbnZhciBfX19DU1NfTE9BREVSX0VYUE9SVF9fXyA9IF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyhfX19DU1NfTE9BREVSX0FQSV9TT1VSQ0VNQVBfSU1QT1JUX19fKTtcbi8vIE1vZHVsZVxuX19fQ1NTX0xPQURFUl9FWFBPUlRfX18ucHVzaChbbW9kdWxlLmlkLCBcIi5jYW1lcmEtdmlldy1yZW5kZXJlcl9idG4ge1xcbiAgYm9yZGVyOiBub25lO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XFxuICB3aWR0aDogMTAwJTtcXG4gIHBhZGRpbmc6IDA7XFxufVxcbi5jYW1lcmEtdmlldy1yZW5kZXJlcl9pY29uIHtcXG4gIHdpZHRoOiAyNXB4O1xcbiAgaGVpZ2h0OiBhdXRvO1xcbn1cIiwgXCJcIix7XCJ2ZXJzaW9uXCI6MyxcInNvdXJjZXNcIjpbXCJ3ZWJwYWNrOi8vLi95b3VyLWV4dGVuc2lvbnMvd2lkZ2V0cy9jYW1lcmFWaWV3UmVuZGVyZXIvc3JjL3J1bnRpbWUvd2lkZ2V0LnNjc3NcIixcIndlYnBhY2s6Ly8uLy4uLy4uLy4uLy4uL0V4cGVyaWVuY2UlMjBCdWlsZGVyL0RpZ2l0YWwlMjBDYXRhbG9ndWUlMjBnaXQvRGlnaXRhbC1DYXRhbG9ndWUvY2xpZW50L3lvdXItZXh0ZW5zaW9ucy93aWRnZXRzL2NhbWVyYVZpZXdSZW5kZXJlci9zcmMvcnVudGltZS93aWRnZXQuc2Nzc1wiXSxcIm5hbWVzXCI6W10sXCJtYXBwaW5nc1wiOlwiQUFDSTtFQUNJLFlBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDQVI7QURFSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDQVJcIixcInNvdXJjZXNDb250ZW50XCI6W1wiLmNhbWVyYS12aWV3LXJlbmRlcmVye1xcclxcbiAgICAmX2J0bntcXHJcXG4gICAgICAgIGJvcmRlcjogbm9uZTtcXHJcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xcclxcbiAgICAgICAgd2lkdGg6IDEwMCU7XFxyXFxuICAgICAgICBwYWRkaW5nOiAwO1xcclxcbiAgICB9XFxyXFxuICAgICZfaWNvbntcXHJcXG4gICAgICAgIHdpZHRoOiAyNXB4O1xcclxcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xcclxcbiAgICB9XFxyXFxufVwiLFwiLmNhbWVyYS12aWV3LXJlbmRlcmVyX2J0biB7XFxuICBib3JkZXI6IG5vbmU7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgcGFkZGluZzogMDtcXG59XFxuLmNhbWVyYS12aWV3LXJlbmRlcmVyX2ljb24ge1xcbiAgd2lkdGg6IDI1cHg7XFxuICBoZWlnaHQ6IGF1dG87XFxufVwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuLy8gRXhwb3J0c1xuZXhwb3J0IGRlZmF1bHQgX19fQ1NTX0xPQURFUl9FWFBPUlRfX187XG4iLCJcInVzZSBzdHJpY3RcIjtcblxuLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcpIHtcbiAgdmFyIGxpc3QgPSBbXTtcblxuICAvLyByZXR1cm4gdGhlIGxpc3Qgb2YgbW9kdWxlcyBhcyBjc3Mgc3RyaW5nXG4gIGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcbiAgICByZXR1cm4gdGhpcy5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgIHZhciBjb250ZW50ID0gXCJcIjtcbiAgICAgIHZhciBuZWVkTGF5ZXIgPSB0eXBlb2YgaXRlbVs1XSAhPT0gXCJ1bmRlZmluZWRcIjtcbiAgICAgIGlmIChpdGVtWzRdKSB7XG4gICAgICAgIGNvbnRlbnQgKz0gXCJAc3VwcG9ydHMgKFwiLmNvbmNhdChpdGVtWzRdLCBcIikge1wiKTtcbiAgICAgIH1cbiAgICAgIGlmIChpdGVtWzJdKSB7XG4gICAgICAgIGNvbnRlbnQgKz0gXCJAbWVkaWEgXCIuY29uY2F0KGl0ZW1bMl0sIFwiIHtcIik7XG4gICAgICB9XG4gICAgICBpZiAobmVlZExheWVyKSB7XG4gICAgICAgIGNvbnRlbnQgKz0gXCJAbGF5ZXJcIi5jb25jYXQoaXRlbVs1XS5sZW5ndGggPiAwID8gXCIgXCIuY29uY2F0KGl0ZW1bNV0pIDogXCJcIiwgXCIge1wiKTtcbiAgICAgIH1cbiAgICAgIGNvbnRlbnQgKz0gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtKTtcbiAgICAgIGlmIChuZWVkTGF5ZXIpIHtcbiAgICAgICAgY29udGVudCArPSBcIn1cIjtcbiAgICAgIH1cbiAgICAgIGlmIChpdGVtWzJdKSB7XG4gICAgICAgIGNvbnRlbnQgKz0gXCJ9XCI7XG4gICAgICB9XG4gICAgICBpZiAoaXRlbVs0XSkge1xuICAgICAgICBjb250ZW50ICs9IFwifVwiO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGNvbnRlbnQ7XG4gICAgfSkuam9pbihcIlwiKTtcbiAgfTtcblxuICAvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxuICBsaXN0LmkgPSBmdW5jdGlvbiBpKG1vZHVsZXMsIG1lZGlhLCBkZWR1cGUsIHN1cHBvcnRzLCBsYXllcikge1xuICAgIGlmICh0eXBlb2YgbW9kdWxlcyA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgbW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgdW5kZWZpbmVkXV07XG4gICAgfVxuICAgIHZhciBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzID0ge307XG4gICAgaWYgKGRlZHVwZSkge1xuICAgICAgZm9yICh2YXIgayA9IDA7IGsgPCB0aGlzLmxlbmd0aDsgaysrKSB7XG4gICAgICAgIHZhciBpZCA9IHRoaXNba11bMF07XG4gICAgICAgIGlmIChpZCAhPSBudWxsKSB7XG4gICAgICAgICAgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIGZvciAodmFyIF9rID0gMDsgX2sgPCBtb2R1bGVzLmxlbmd0aDsgX2srKykge1xuICAgICAgdmFyIGl0ZW0gPSBbXS5jb25jYXQobW9kdWxlc1tfa10pO1xuICAgICAgaWYgKGRlZHVwZSAmJiBhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2l0ZW1bMF1dKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBsYXllciAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICBpZiAodHlwZW9mIGl0ZW1bNV0gPT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICBpdGVtWzVdID0gbGF5ZXI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaXRlbVsxXSA9IFwiQGxheWVyXCIuY29uY2F0KGl0ZW1bNV0ubGVuZ3RoID4gMCA/IFwiIFwiLmNvbmNhdChpdGVtWzVdKSA6IFwiXCIsIFwiIHtcIikuY29uY2F0KGl0ZW1bMV0sIFwifVwiKTtcbiAgICAgICAgICBpdGVtWzVdID0gbGF5ZXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChtZWRpYSkge1xuICAgICAgICBpZiAoIWl0ZW1bMl0pIHtcbiAgICAgICAgICBpdGVtWzJdID0gbWVkaWE7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaXRlbVsxXSA9IFwiQG1lZGlhIFwiLmNvbmNhdChpdGVtWzJdLCBcIiB7XCIpLmNvbmNhdChpdGVtWzFdLCBcIn1cIik7XG4gICAgICAgICAgaXRlbVsyXSA9IG1lZGlhO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAoc3VwcG9ydHMpIHtcbiAgICAgICAgaWYgKCFpdGVtWzRdKSB7XG4gICAgICAgICAgaXRlbVs0XSA9IFwiXCIuY29uY2F0KHN1cHBvcnRzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpdGVtWzFdID0gXCJAc3VwcG9ydHMgKFwiLmNvbmNhdChpdGVtWzRdLCBcIikge1wiKS5jb25jYXQoaXRlbVsxXSwgXCJ9XCIpO1xuICAgICAgICAgIGl0ZW1bNF0gPSBzdXBwb3J0cztcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgbGlzdC5wdXNoKGl0ZW0pO1xuICAgIH1cbiAgfTtcbiAgcmV0dXJuIGxpc3Q7XG59OyIsIlwidXNlIHN0cmljdFwiO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdGVtKSB7XG4gIHZhciBjb250ZW50ID0gaXRlbVsxXTtcbiAgdmFyIGNzc01hcHBpbmcgPSBpdGVtWzNdO1xuICBpZiAoIWNzc01hcHBpbmcpIHtcbiAgICByZXR1cm4gY29udGVudDtcbiAgfVxuICBpZiAodHlwZW9mIGJ0b2EgPT09IFwiZnVuY3Rpb25cIikge1xuICAgIHZhciBiYXNlNjQgPSBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShjc3NNYXBwaW5nKSkpKTtcbiAgICB2YXIgZGF0YSA9IFwic291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsXCIuY29uY2F0KGJhc2U2NCk7XG4gICAgdmFyIHNvdXJjZU1hcHBpbmcgPSBcIi8qIyBcIi5jb25jYXQoZGF0YSwgXCIgKi9cIik7XG4gICAgdmFyIHNvdXJjZVVSTHMgPSBjc3NNYXBwaW5nLnNvdXJjZXMubWFwKGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICAgIHJldHVybiBcIi8qIyBzb3VyY2VVUkw9XCIuY29uY2F0KGNzc01hcHBpbmcuc291cmNlUm9vdCB8fCBcIlwiKS5jb25jYXQoc291cmNlLCBcIiAqL1wiKTtcbiAgICB9KTtcbiAgICByZXR1cm4gW2NvbnRlbnRdLmNvbmNhdChzb3VyY2VVUkxzKS5jb25jYXQoW3NvdXJjZU1hcHBpbmddKS5qb2luKFwiXFxuXCIpO1xuICB9XG4gIHJldHVybiBbY29udGVudF0uam9pbihcIlxcblwiKTtcbn07IiwiXG4gICAgICBpbXBvcnQgQVBJIGZyb20gXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCI7XG4gICAgICBpbXBvcnQgZG9tQVBJIGZyb20gXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvc3R5bGVEb21BUEkuanNcIjtcbiAgICAgIGltcG9ydCBpbnNlcnRGbiBmcm9tIFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luc2VydEJ5U2VsZWN0b3IuanNcIjtcbiAgICAgIGltcG9ydCBzZXRBdHRyaWJ1dGVzIGZyb20gXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvc2V0QXR0cmlidXRlc1dpdGhvdXRBdHRyaWJ1dGVzLmpzXCI7XG4gICAgICBpbXBvcnQgaW5zZXJ0U3R5bGVFbGVtZW50IGZyb20gXCIhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5zZXJ0U3R5bGVFbGVtZW50LmpzXCI7XG4gICAgICBpbXBvcnQgc3R5bGVUYWdUcmFuc2Zvcm1GbiBmcm9tIFwiIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL3N0eWxlVGFnVHJhbnNmb3JtLmpzXCI7XG4gICAgICBpbXBvcnQgY29udGVudCwgKiBhcyBuYW1lZEV4cG9ydCBmcm9tIFwiISEuLi8uLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cnVsZVNldFsxXS5ydWxlc1szXS51c2VbMV0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Jlc29sdmUtdXJsLWxvYWRlci9pbmRleC5qcz8/cnVsZVNldFsxXS5ydWxlc1szXS51c2VbMl0hLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Nhc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9ydWxlU2V0WzFdLnJ1bGVzWzNdLnVzZVszXSEuL3dpZGdldC5zY3NzXCI7XG4gICAgICBcbiAgICAgIFxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLnN0eWxlVGFnVHJhbnNmb3JtID0gc3R5bGVUYWdUcmFuc2Zvcm1Gbjtcbm9wdGlvbnMuc2V0QXR0cmlidXRlcyA9IHNldEF0dHJpYnV0ZXM7XG5cbiAgICAgIG9wdGlvbnMuaW5zZXJ0ID0gaW5zZXJ0Rm4uYmluZChudWxsLCBcImhlYWRcIik7XG4gICAgXG5vcHRpb25zLmRvbUFQSSA9IGRvbUFQSTtcbm9wdGlvbnMuaW5zZXJ0U3R5bGVFbGVtZW50ID0gaW5zZXJ0U3R5bGVFbGVtZW50O1xuXG52YXIgdXBkYXRlID0gQVBJKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5cblxuZXhwb3J0ICogZnJvbSBcIiEhLi4vLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9janMuanM/P3J1bGVTZXRbMV0ucnVsZXNbM10udXNlWzFdIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9yZXNvbHZlLXVybC1sb2FkZXIvaW5kZXguanM/P3J1bGVTZXRbMV0ucnVsZXNbM10udXNlWzJdIS4uLy4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cnVsZVNldFsxXS5ydWxlc1szXS51c2VbM10hLi93aWRnZXQuc2Nzc1wiO1xuICAgICAgIGV4cG9ydCBkZWZhdWx0IGNvbnRlbnQgJiYgY29udGVudC5sb2NhbHMgPyBjb250ZW50LmxvY2FscyA6IHVuZGVmaW5lZDtcbiIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgc3R5bGVzSW5ET00gPSBbXTtcblxuZnVuY3Rpb24gZ2V0SW5kZXhCeUlkZW50aWZpZXIoaWRlbnRpZmllcikge1xuICB2YXIgcmVzdWx0ID0gLTE7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXNJbkRPTS5sZW5ndGg7IGkrKykge1xuICAgIGlmIChzdHlsZXNJbkRPTVtpXS5pZGVudGlmaWVyID09PSBpZGVudGlmaWVyKSB7XG4gICAgICByZXN1bHQgPSBpO1xuICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZnVuY3Rpb24gbW9kdWxlc1RvRG9tKGxpc3QsIG9wdGlvbnMpIHtcbiAgdmFyIGlkQ291bnRNYXAgPSB7fTtcbiAgdmFyIGlkZW50aWZpZXJzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldO1xuICAgIHZhciBpZCA9IG9wdGlvbnMuYmFzZSA/IGl0ZW1bMF0gKyBvcHRpb25zLmJhc2UgOiBpdGVtWzBdO1xuICAgIHZhciBjb3VudCA9IGlkQ291bnRNYXBbaWRdIHx8IDA7XG4gICAgdmFyIGlkZW50aWZpZXIgPSBcIlwiLmNvbmNhdChpZCwgXCIgXCIpLmNvbmNhdChjb3VudCk7XG4gICAgaWRDb3VudE1hcFtpZF0gPSBjb3VudCArIDE7XG4gICAgdmFyIGluZGV4QnlJZGVudGlmaWVyID0gZ2V0SW5kZXhCeUlkZW50aWZpZXIoaWRlbnRpZmllcik7XG4gICAgdmFyIG9iaiA9IHtcbiAgICAgIGNzczogaXRlbVsxXSxcbiAgICAgIG1lZGlhOiBpdGVtWzJdLFxuICAgICAgc291cmNlTWFwOiBpdGVtWzNdLFxuICAgICAgc3VwcG9ydHM6IGl0ZW1bNF0sXG4gICAgICBsYXllcjogaXRlbVs1XVxuICAgIH07XG5cbiAgICBpZiAoaW5kZXhCeUlkZW50aWZpZXIgIT09IC0xKSB7XG4gICAgICBzdHlsZXNJbkRPTVtpbmRleEJ5SWRlbnRpZmllcl0ucmVmZXJlbmNlcysrO1xuICAgICAgc3R5bGVzSW5ET01baW5kZXhCeUlkZW50aWZpZXJdLnVwZGF0ZXIob2JqKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIHVwZGF0ZXIgPSBhZGRFbGVtZW50U3R5bGUob2JqLCBvcHRpb25zKTtcbiAgICAgIG9wdGlvbnMuYnlJbmRleCA9IGk7XG4gICAgICBzdHlsZXNJbkRPTS5zcGxpY2UoaSwgMCwge1xuICAgICAgICBpZGVudGlmaWVyOiBpZGVudGlmaWVyLFxuICAgICAgICB1cGRhdGVyOiB1cGRhdGVyLFxuICAgICAgICByZWZlcmVuY2VzOiAxXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZGVudGlmaWVycy5wdXNoKGlkZW50aWZpZXIpO1xuICB9XG5cbiAgcmV0dXJuIGlkZW50aWZpZXJzO1xufVxuXG5mdW5jdGlvbiBhZGRFbGVtZW50U3R5bGUob2JqLCBvcHRpb25zKSB7XG4gIHZhciBhcGkgPSBvcHRpb25zLmRvbUFQSShvcHRpb25zKTtcbiAgYXBpLnVwZGF0ZShvYmopO1xuXG4gIHZhciB1cGRhdGVyID0gZnVuY3Rpb24gdXBkYXRlcihuZXdPYmopIHtcbiAgICBpZiAobmV3T2JqKSB7XG4gICAgICBpZiAobmV3T2JqLmNzcyA9PT0gb2JqLmNzcyAmJiBuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJiBuZXdPYmouc291cmNlTWFwID09PSBvYmouc291cmNlTWFwICYmIG5ld09iai5zdXBwb3J0cyA9PT0gb2JqLnN1cHBvcnRzICYmIG5ld09iai5sYXllciA9PT0gb2JqLmxheWVyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgYXBpLnVwZGF0ZShvYmogPSBuZXdPYmopO1xuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVtb3ZlKCk7XG4gICAgfVxuICB9O1xuXG4gIHJldHVybiB1cGRhdGVyO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChsaXN0LCBvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICBsaXN0ID0gbGlzdCB8fCBbXTtcbiAgdmFyIGxhc3RJZGVudGlmaWVycyA9IG1vZHVsZXNUb0RvbShsaXN0LCBvcHRpb25zKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZShuZXdMaXN0KSB7XG4gICAgbmV3TGlzdCA9IG5ld0xpc3QgfHwgW107XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxhc3RJZGVudGlmaWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGlkZW50aWZpZXIgPSBsYXN0SWRlbnRpZmllcnNbaV07XG4gICAgICB2YXIgaW5kZXggPSBnZXRJbmRleEJ5SWRlbnRpZmllcihpZGVudGlmaWVyKTtcbiAgICAgIHN0eWxlc0luRE9NW2luZGV4XS5yZWZlcmVuY2VzLS07XG4gICAgfVxuXG4gICAgdmFyIG5ld0xhc3RJZGVudGlmaWVycyA9IG1vZHVsZXNUb0RvbShuZXdMaXN0LCBvcHRpb25zKTtcblxuICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBsYXN0SWRlbnRpZmllcnMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgX2lkZW50aWZpZXIgPSBsYXN0SWRlbnRpZmllcnNbX2ldO1xuXG4gICAgICB2YXIgX2luZGV4ID0gZ2V0SW5kZXhCeUlkZW50aWZpZXIoX2lkZW50aWZpZXIpO1xuXG4gICAgICBpZiAoc3R5bGVzSW5ET01bX2luZGV4XS5yZWZlcmVuY2VzID09PSAwKSB7XG4gICAgICAgIHN0eWxlc0luRE9NW19pbmRleF0udXBkYXRlcigpO1xuXG4gICAgICAgIHN0eWxlc0luRE9NLnNwbGljZShfaW5kZXgsIDEpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGxhc3RJZGVudGlmaWVycyA9IG5ld0xhc3RJZGVudGlmaWVycztcbiAgfTtcbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBtZW1vID0ge307XG4vKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAgKi9cblxuZnVuY3Rpb24gZ2V0VGFyZ2V0KHRhcmdldCkge1xuICBpZiAodHlwZW9mIG1lbW9bdGFyZ2V0XSA9PT0gXCJ1bmRlZmluZWRcIikge1xuICAgIHZhciBzdHlsZVRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTsgLy8gU3BlY2lhbCBjYXNlIHRvIHJldHVybiBoZWFkIG9mIGlmcmFtZSBpbnN0ZWFkIG9mIGlmcmFtZSBpdHNlbGZcblxuICAgIGlmICh3aW5kb3cuSFRNTElGcmFtZUVsZW1lbnQgJiYgc3R5bGVUYXJnZXQgaW5zdGFuY2VvZiB3aW5kb3cuSFRNTElGcmFtZUVsZW1lbnQpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIC8vIFRoaXMgd2lsbCB0aHJvdyBhbiBleGNlcHRpb24gaWYgYWNjZXNzIHRvIGlmcmFtZSBpcyBibG9ja2VkXG4gICAgICAgIC8vIGR1ZSB0byBjcm9zcy1vcmlnaW4gcmVzdHJpY3Rpb25zXG4gICAgICAgIHN0eWxlVGFyZ2V0ID0gc3R5bGVUYXJnZXQuY29udGVudERvY3VtZW50LmhlYWQ7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIC8vIGlzdGFuYnVsIGlnbm9yZSBuZXh0XG4gICAgICAgIHN0eWxlVGFyZ2V0ID0gbnVsbDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBtZW1vW3RhcmdldF0gPSBzdHlsZVRhcmdldDtcbiAgfVxuXG4gIHJldHVybiBtZW1vW3RhcmdldF07XG59XG4vKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAgKi9cblxuXG5mdW5jdGlvbiBpbnNlcnRCeVNlbGVjdG9yKGluc2VydCwgc3R5bGUpIHtcbiAgdmFyIHRhcmdldCA9IGdldFRhcmdldChpbnNlcnQpO1xuXG4gIGlmICghdGFyZ2V0KSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFwiQ291bGRuJ3QgZmluZCBhIHN0eWxlIHRhcmdldC4gVGhpcyBwcm9iYWJseSBtZWFucyB0aGF0IHRoZSB2YWx1ZSBmb3IgdGhlICdpbnNlcnQnIHBhcmFtZXRlciBpcyBpbnZhbGlkLlwiKTtcbiAgfVxuXG4gIHRhcmdldC5hcHBlbmRDaGlsZChzdHlsZSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaW5zZXJ0QnlTZWxlY3RvcjsiLCJcInVzZSBzdHJpY3RcIjtcblxuLyogaXN0YW5idWwgaWdub3JlIG5leHQgICovXG5mdW5jdGlvbiBpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucykge1xuICB2YXIgZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzdHlsZVwiKTtcbiAgb3B0aW9ucy5zZXRBdHRyaWJ1dGVzKGVsZW1lbnQsIG9wdGlvbnMuYXR0cmlidXRlcyk7XG4gIG9wdGlvbnMuaW5zZXJ0KGVsZW1lbnQsIG9wdGlvbnMub3B0aW9ucyk7XG4gIHJldHVybiBlbGVtZW50O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGluc2VydFN0eWxlRWxlbWVudDsiLCJcInVzZSBzdHJpY3RcIjtcblxuLyogaXN0YW5idWwgaWdub3JlIG5leHQgICovXG5mdW5jdGlvbiBzZXRBdHRyaWJ1dGVzV2l0aG91dEF0dHJpYnV0ZXMoc3R5bGVFbGVtZW50KSB7XG4gIHZhciBub25jZSA9IHR5cGVvZiBfX3dlYnBhY2tfbm9uY2VfXyAhPT0gXCJ1bmRlZmluZWRcIiA/IF9fd2VicGFja19ub25jZV9fIDogbnVsbDtcblxuICBpZiAobm9uY2UpIHtcbiAgICBzdHlsZUVsZW1lbnQuc2V0QXR0cmlidXRlKFwibm9uY2VcIiwgbm9uY2UpO1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gc2V0QXR0cmlidXRlc1dpdGhvdXRBdHRyaWJ1dGVzOyIsIlwidXNlIHN0cmljdFwiO1xuXG4vKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAgKi9cbmZ1bmN0aW9uIGFwcGx5KHN0eWxlRWxlbWVudCwgb3B0aW9ucywgb2JqKSB7XG4gIHZhciBjc3MgPSBcIlwiO1xuXG4gIGlmIChvYmouc3VwcG9ydHMpIHtcbiAgICBjc3MgKz0gXCJAc3VwcG9ydHMgKFwiLmNvbmNhdChvYmouc3VwcG9ydHMsIFwiKSB7XCIpO1xuICB9XG5cbiAgaWYgKG9iai5tZWRpYSkge1xuICAgIGNzcyArPSBcIkBtZWRpYSBcIi5jb25jYXQob2JqLm1lZGlhLCBcIiB7XCIpO1xuICB9XG5cbiAgdmFyIG5lZWRMYXllciA9IHR5cGVvZiBvYmoubGF5ZXIgIT09IFwidW5kZWZpbmVkXCI7XG5cbiAgaWYgKG5lZWRMYXllcikge1xuICAgIGNzcyArPSBcIkBsYXllclwiLmNvbmNhdChvYmoubGF5ZXIubGVuZ3RoID4gMCA/IFwiIFwiLmNvbmNhdChvYmoubGF5ZXIpIDogXCJcIiwgXCIge1wiKTtcbiAgfVxuXG4gIGNzcyArPSBvYmouY3NzO1xuXG4gIGlmIChuZWVkTGF5ZXIpIHtcbiAgICBjc3MgKz0gXCJ9XCI7XG4gIH1cblxuICBpZiAob2JqLm1lZGlhKSB7XG4gICAgY3NzICs9IFwifVwiO1xuICB9XG5cbiAgaWYgKG9iai5zdXBwb3J0cykge1xuICAgIGNzcyArPSBcIn1cIjtcbiAgfVxuXG4gIHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwO1xuXG4gIGlmIChzb3VyY2VNYXAgJiYgdHlwZW9mIGJ0b2EgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICBjc3MgKz0gXCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiLmNvbmNhdChidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpLCBcIiAqL1wiKTtcbiAgfSAvLyBGb3Igb2xkIElFXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIGlmICAqL1xuXG5cbiAgb3B0aW9ucy5zdHlsZVRhZ1RyYW5zZm9ybShjc3MsIHN0eWxlRWxlbWVudCwgb3B0aW9ucy5vcHRpb25zKTtcbn1cblxuZnVuY3Rpb24gcmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlRWxlbWVudCkge1xuICAvLyBpc3RhbmJ1bCBpZ25vcmUgaWZcbiAgaWYgKHN0eWxlRWxlbWVudC5wYXJlbnROb2RlID09PSBudWxsKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgc3R5bGVFbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3R5bGVFbGVtZW50KTtcbn1cbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICAqL1xuXG5cbmZ1bmN0aW9uIGRvbUFQSShvcHRpb25zKSB7XG4gIHZhciBzdHlsZUVsZW1lbnQgPSBvcHRpb25zLmluc2VydFN0eWxlRWxlbWVudChvcHRpb25zKTtcbiAgcmV0dXJuIHtcbiAgICB1cGRhdGU6IGZ1bmN0aW9uIHVwZGF0ZShvYmopIHtcbiAgICAgIGFwcGx5KHN0eWxlRWxlbWVudCwgb3B0aW9ucywgb2JqKTtcbiAgICB9LFxuICAgIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKCkge1xuICAgICAgcmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlRWxlbWVudCk7XG4gICAgfVxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGRvbUFQSTsiLCJcInVzZSBzdHJpY3RcIjtcblxuLyogaXN0YW5idWwgaWdub3JlIG5leHQgICovXG5mdW5jdGlvbiBzdHlsZVRhZ1RyYW5zZm9ybShjc3MsIHN0eWxlRWxlbWVudCkge1xuICBpZiAoc3R5bGVFbGVtZW50LnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZUVsZW1lbnQuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzO1xuICB9IGVsc2Uge1xuICAgIHdoaWxlIChzdHlsZUVsZW1lbnQuZmlyc3RDaGlsZCkge1xuICAgICAgc3R5bGVFbGVtZW50LnJlbW92ZUNoaWxkKHN0eWxlRWxlbWVudC5maXJzdENoaWxkKTtcbiAgICB9XG5cbiAgICBzdHlsZUVsZW1lbnQuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKSk7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzdHlsZVRhZ1RyYW5zZm9ybTsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFnQUFBQUlBQ0FZQUFBRDBlTlQ2QUFBQUJITkNTVlFJQ0FnSWZBaGtpQUFBQUFsd1NGbHpBQUFPeEFBQURzUUJsU3NPR3dBQUFCbDBSVmgwVTI5bWRIZGhjbVVBZDNkM0xtbHVhM05qWVhCbExtOXlaNXZ1UEJvQUFDQUFTVVJCVkhpYzdkMTUyRjFWZWJEeE8zT1l3aEJtUWtpUUVHWkVFRkVRRUNpRGdEamlWSkVxaUhXMkR0aldydzdWaWpOUXE4WFcxbEpiWlZDcm9ESW9JREpJRVpSNWxDUVFKaGtDSVF5WnZ6K2U4elloSk9HYzg1NjExOTU3M2IvcmVxNDNnV1R0NSt5emM5WnoxbDU3TFpBa1NaSWtTWklrU1pJa1NaSWtTWklrU1pJa1NaSWtTWklrU1pJa1NaSWtTWklrU1pJa1NaSWtTWklrU1pJa1NaSWtTWklrU1pJa1NaSWtTWklrcVNWRzVFNUFhcUJOZ2VuQXRzQld3QVJnTFdDZG5Fa1Y1QW5nU1dBdU1CTzRBN2dWZURCalRsTGpXQUJJejI5RDRFaGdmMkEvb3ROWC9jd0VmZzFjQXB3RFBKSXpHVWxTTTQwQmppWTZrZ1hBVXFOUk1SLzRLZkNHem5zcFNkSnFqUU9PSVlhVmMzZGl4bUJpSnZCQllFMGtTVnJCQ09CdHdQM2s3N0NNTkhFdjhGYTg5U2xKNnRnZXVKVDhIWlJSVFZ3Q2JJY2txV2pIQVBQSTN5a1oxY1pUeEcwQlNWSmh4Z0duazc4ak12TEdkNGxyUVpKVWdMV0I4OG5mK1JqMWlJdUJkWkVrdGRyR3dMWGs3M1NNZXNXMXhMVWhGY1Bac0NySnhzQ3ZnSjF5SjZKYXVoVTRnSGdTUkdvOUN3Q1Z3czVmM2JBSVVERXNBRlFDTzMvMXdpSkFSYkFBVU52Withc2ZGZ0ZxUFFzQXRabWR2NGJESWtDdFpnR2d0ckx6MXlCWUJLaTFMQURVUm5iK0dpU0xBTFdTQllEYXhzNWZLVmdFcUhVc0FOUW1kdjVLeVNKQXJXSUJvTGF3ODFjVkxBTFVHaFlBYWdNN2YxWEpJa0N0WUFHZ3ByUHpWdzRXQVdvOEN3QTFtWjIvY3JJSVVLTlpBS2lwN1B4VkJ4WUJhaXdMQURXUm5iL3F4Q0pBaldRQm9LYXg4MWNkV1FTb2NTd0ExQ1IyL3Fveml3QTFpZ1dBbXNMT1gwMWdFYURHc0FCUUU5ajVxMGtzQXRRSUZnQ3FPenQvTlpGRmdHclBBa0IxWnVldkpyTUlVSzFaQUtpdTdQelZCaFlCcWkwTEFOV1JuYi9heENKQXRXUUJvTHF4ODFjYldRU29kaXdBVkNkMi9tb3ppd0RWaWdXQTZzTE9YeVd3Q0ZCdFdBQ29EdXo4VlJLTEFOV0NCWUJ5cy9OWGlTd0NsSjBGZ0hLeTgxZkpMQUtVMWNqY0NhaFltd0FYWWVldmNtMEhYRUFVd2xMbExBQ1V3eTdBbGNDT3VST1JNdHNKdUFiWVBYY2lLcytvM0Ftb0tHc0Rud0QrSGIvMVNFTW1BRzhsdnBCZEN5ekltNDVLNFJ3QXBiUUdzQVV4MUhrRThGcGdvNndaU2ZYMkVQQWo0QnhpanNCOXdOTlpNMUpyV1FEVXd6aGdaMkJib3JQY0ZwZ0NyRXQ4YTE0TFdEOVhjcExVb3puQVBPQko0SEZnSm5BYlVkVGNEdHdJek0rVm5JSUZRQjZqZ0JjRHJ5Qm1BZTlOZkZ1V3BCSThCVnhPVEFTK0dMZ2FXSkkxb3dKWkFGUnJlK0NOd051SmIvaVNwTGpWY1Rid1hlRDNlVk1waHdWQWVtT0F0d0FmQUY2VU9SZEpxcnRyZ0ZPQTd3T0xNdWZTYWhZQTZZd0YzZ1I4RXBpV09SZEphcHBad05lQmIrTkV5Q1FzQU5KNEUvQVZZZ2E4SktsLzl3QWZBYzdLblVqYldBQU0xamJBTjRCRGNpY2lTUzF6TWZCZTRKYmNpYlNGQ3dFTnhraGlnWnV6Z09tWmM1R2tOcG9LSEVjc2xIUkY1bHhhd1JHQTRkc1FPQjA0TEhjaWtsU0lYd0ovRGp5WU81RW1zd0FZbnBjRFB3QTJ6NTJJSkJWbU52Rll0YU1CZlhJem9QNGRSZXprWmVjdlNkV2JSQ3drOVByY2lUU1Zjd0Q2Y3l6d1BlSlJQMGxTSHFPQjF4RzNBcTdKbkV2aldBRDA3a1JpcHIrako1S1UzMGpnY0dKNVlXOEg5TUFDb0RjbkFDZm5Ua0tTOUN3amdJT0FlNGt0bGRVRkM0RHVIUVg4QjM3emw2UTZHa0dNQk55RWF3VjB4YWNBdXZOeTRFSmkyMTVKVW4wOVEreXllbVh1Uk9yT0F1RDViVVRzVHVXeXZwTFVETE9CM1lDSGN5ZFNadzVucjk0STREdlkrVXRTazB3aUZtanpTKzVxT0FkZzlUNEJ2Q2QzRXBLa25rMERuc1FuQTFiSjZtalZwZ0UzNEgxL1NXcXErY0N1d0cyNUU2a2pid0dzMmluWStVdFNrNDBEVHMyZFJGMVpBS3pjbTNCekgwbHFnNE9KMVFLMUFtOEJQTmQ0NEU2YytDZEpiWEUzY1Z0M1FlNUU2c1FSZ09mNkMrejhKYWxOSmdOdno1MUUzVGdDOEd4amlNa2lVM01uSWtrYXFMdUE2Y0NpM0luVWhTTUF6L1lXN1B3bHFZMjJKdVozcWNNUmdHZTdCbmhSN2lRa1NVbGNEZXlaTzRtNnNBQllaZ2RpRXdsSlVudnRoSi8xZ0xjQWx1Y0VFVWxxdjdma1RxQXVIQUVJSTRGWnhQclJrcVQydWhmWUNsaWNPNUhjSEFFSWUyTG5MMGtsMkFKNFllNGs2c0FDSUJ5UU93RkpVbVVPekoxQUhWZ0FoRmZrVGtDU1ZCay84M0VPQU1CWVlBNndadTVFSkVtVmVBcllnTmd0c0ZpT0FNRE8yUGxMVWtuV0pCNzlMcG9GQUd5WE93RkpVdVdtNTA0Z053c0FMd0pKS2xIeG4vMFdBTEJ0N2dRa1NaVXIvclBmQWdDbTVFNUFrbFM1clhNbmtKc0ZBS3liT3dGSlV1VW01RTRnTndzQVdDZDNBcEtreWhYLzJXOEI0RVVnU1NVcS9yUGZoWUJnRVRBcWR4SlNRbzhERDNWK1BnWXNCUllDOHpyL2YyMWdEUEY1c0I1eFcyd2p2RDNXUnM4QWR3SVBBRTkyL3R0YXdLYkFOR0JjcHJ4eVdBeU16cDFFVGhZQThXRW9OZG5Ed0hYQWJjQU1ZR1luN2dFZUlZcmNmb3dHSmdLVGljbXlRekVkMkJYWXNOK0VWWmtGd00rQjg0QkxnRHVBSmF2NHN5T0ptZkd2QUE0QkRpTldTbTJ6b3Z2QW9sOThod1dBbXVSUjRFcmdDdUFhNEhyZy9reTViRTZzcExrSDhGTGdaY0Q2bVhMUnM5MExmQTM0TG5ITjlHTWljQ3p3VjhSNzNVWkY5NEZGdi9nT0N3RFYyVHpnVjhRM3VFdUJXNmp2TlRzUzJCN1lsL2oyZUFBeHZLenF6QVUrRFh5VHdhMXpQdzU0UC9BcDRuWlJteFRkQnhiOTRqdnErbUdxY3QwRG5BMmNDMXhHRE9NMjBUamc1Y0Rod091QlNYblRhYjBMaVcvczl5VnFmeEx3SDdSciszVDd3TUl0Tll3YXhQM0FLY0RldFBORGFTUlJEUHdqTVFFdDkvbHVVeXdCUGtNMVQzV05BajVYOGV0TEdTcGM3Z3ZRS0RjV0V4TzBYazFaczVISEFLOERma0djZzl6dlE1TmpFWEI4YjZkL0lFN29IRHYzNng5dXFIQzVMMENqdkhnRStBZGlkbjNwcGdCZklDYXE1WDVmbWhaTHlOUDVEemxoRlhrMUtWUzQzQmVnVVU3Y0Nid1BKOGF0ek5yQUI0Qzd5UDgrTlNVKzA5ZVpIcXltM3c1UTRYSmZnRWI3NHc3Z3ozSEJxVzZNQW80aGlxWGM3MXVkNHdMcXNaTHJLT0FpOHArUGZrT0Z5MzBCR3UyTm1jQTdLZXYrL3FDTUlZYTNaNUgvZmF4YlBFNjluc3ZmRW5pQy9PZWxuMURoY2wrQVJ2dGlMbkFpWlMycm1zcDQ0SzlwYmdlVElqNDhyRE9heHNmSmYxNzZDUlV1OXdWb3RDZVdBS2NUNjZwcnNEWURUcU1kTTgrSEUvZFF6OEp5UExINllPN3owMnVvY0xrdlFLTWRjU093RjBwdGIrQm04ci9mdWFLTzMvNkhmSlQ4NTZmWFVPRnlYNEJHczJNQmNCTDEvRmJXVm1PSVd5elBrUC85cnpMbVUrOE5tQ1lTT2VZK1Q3MkVDcGY3QWpTYUc3OEhka1M1N0V6c2dwajdPcWdxZmp5WTA1YlVPZVEvVDcxRTBlcndHSW5VTkV1QlU0a2gvNXN5NTFLeUc0QTlnUyt5NmkxdTIrUzgzQWwwb1FrNVN2OG5kd1ZxTkN2dUJ3NUNkWE1JOENENXI0K1VNWDFnWnl1ZEhjaC9ubnFKb3JWeDA1RmVGWDhScUd1WEEwZVRicmMxRGM4V3dGbkFTM01ua3NBenhHcUppM01uOGp4R0VWdFlqOCtkU0plSzdnTzlCU0IxNXh2RU5xaDIvdlYxTDdBLzhLM01lYVJ3Qi9Ydi9DRnkvR1B1Sk5RZEN3QnA5UllDeHdIdkoyYjhxOTRXQU84QjNrV3NHZEFXRCtWT29BZC95cDJBdW1NQklLM2FQT0FvNER1NUUxSFAvZ1U0akZnMnR3MmV5SjFBRDVxVWE5RmNvMXhhdVh1QXc0bVo1azB5QXBoS1RNYWFTbXkzT3huWWlIaE9leUp4ZjNZc3kzWWxmSkw0NXZ3TXNWWHhJOFMzdUh1SS9RenVBbTRCWnRDc09UTy9CUFlGZmdaTXlwekxjQlY5cjFwcFdBQkl6elVET0xEenMrNjJJRmJIMnh0NE1iQVRzRTZQYmF6RnNtSmdzOVg4dVNlSWd1aHFZa0xrNWRSL1RzVDF4TG41SlRBdGN5N0QwYVF0cENma1RrRHFWdTdIVUl4NnhZMnN2aFBNYlR4d0tIQUtjQnY1ejlldHdOZUp4L0RxdkJyaTVzU2FEYm5QVjc5eC9lQlBTVEkza3Y5OGRSc3FYTzRMMEtoUFhFczlsMW9kQ3h4QmJEVDBPUG5QMDZyaU1lQS9pRnNuWTVLY2llSFppRmk5TWZkNTZpZWVwaGx6dGtiUnJDV2FWYmpjRjZCUmo3aVIrblgrMDRoOUJoNGcvL25wTlI0Z1JpbnF0bFR5UmpSM0pHQzdCT2RqMEZ3SVNJMlMrd0kwOHNjZHhCQnhYZXhEckttK2hQem5aaEJ4R1hBazlabkl0Z2x4NnlMM2VlazEzcDNpWkF6WSs4aC9ubm9KRlM3M0JXamtqYnVKV2ZLNWpRQmVRM09IcUx1SmE0akhLdXRRQ0d4RlBPV1ErNXowRXYrVDVFd00xcm5rUDArOWhBcVgrd0kwOHNWY1lGZnlPd3o0SGZuUFIxVnhOVEZwTUxjZGlYa0x1YzlIdDFIMzdZQTNJaDRuelgyZWVna1ZMdmNGYU9TSkJjQ2ZrZGQwbXZlTmFaQnhJZkhZWWs2SEVLczk1ajRYM2NaSDBweUdnZmdZK2M5UHI2SEM1YjRBalR6eFR2SlpFL2dxemVwNFVzVUNZanZmTllaMVJvZm5YU3ZKcTY1eEwvVjgzSElOWXFmTTNPZW4xMURoY2wrQVJ2WHhEZkk1aU5nc0pmYzVxRnZjUVd5MmxNdTNWcEZYSGVPamljN0JjSHlDL09lbG4xRGhjbCtBUnJWeE9YbWVVUjlQUE5LM3VNZDhTNG9sd0duRUNFblZ4Z0pYOXBodnJwaEx2Wlkybmt6c201SDd2UFFUS2x6dUM5Q29MaDRnbHM2dDJpNDBhM1cwM0hFOWVkWVAySlJZMmpqMzYrOG1MaVlXM2NsdEpMSE1jdTd6MFcrb2NMa3ZRS09hV0V3TXYxZnR6VFQzMjFIT2VBcDRleC9uZTdnT3BqbnJMM3d1MFRub3hVbmtQdy9EQ1JVdTl3Vm9WQk5mcGxxamlEWHljNy91cHNkWHFINEozQ2E5Yis5UGRBNjZjY0pxOG1wS3FIQzVMMEFqZmZ5ZWFtZE9qd1hPU1BBNlNvMGZFWE1vcWpJT3VDN0I2MGdSaTRpT3VHcnY3aHc3OStzZmJxaHd1UzlBSTIwOFE3WDNrOWNHemsvd09rcVBpNmgybTltZGlZVjNjci91YnVOelZETW5ZQlROSC9aZlBsUzQzQmVna1RZK1JYVTJJWmE3emYyYTJ4cS9BemJ1K3QwWXZzOG1laDJwNGlKZ3l5Um5Ja3dHTHFuQjZ4eGtxSEM1TDBBalhkeElETWRYWVFyeExIdnUxOXoydUwxenJxc3dEcmdsOGVzWmREd0JmSnpCM2pKWmczak92NDJUV1ZXNDNCZWdrU1lXQTN0UmpZMW81dTV5VFkwN2lVZjJxckFQelhrcVlQbTRqMWd3YURoN0IyeEVMTy9ieEJYK3VnMFZMdmNGYUtTSjcxQ05DVGpzbnlPdUE5YnI0djBaaE5NcmVrMHBZZ0h3VStDOXdQYXNmcDdBS0dBSFlrdmZjMm5leGo3OVJOSHFzQzFuYnNWZkJDMzBCTEF0c2ZCUFNtT0pEOHJjbXdxVjZtSmlKOFg1aVkrek9YQWJNY0d6NmVZVFMxRS9RQXpwUTd5dVRZRnRxTzZXV1YzWUJ4WXVkd1ZxREQ0K1Rub2pnVE16dlQ1aldmeVlhbWEvLzIybTEyZWtqYUpaL1hnUnRNMHNZcHZkMU44S3Z3NThLUEV4MUowdkFTY21Qc1o0WWdKaXlsbjJxbDdSZldEVksyeEpxZjA5NlR2L3QyRG5YeWNmQTQ1S2ZJeG5nTThuUG9aVXFhS3JudzVIQU5yalRtS2kwNktFeDlpRjJEVXV4NDUxV3JWSGdOMkFleEllWXd6eHRNZldDWStoYWhYZEJ6b0NvRGI1TkdrNy8vSEE5N0R6cjZPSndGbWszZXA1SVRIQ0pMV0NCWURhWWdid2c4VEgrREt4Ukt6cTZTV2s3NkQvRTdncjhUR2tTbGdBcUMyK1NpeitrOHBCeExQVXFyZVBBL3NsYkg4eGNHckM5cVhLRkgzL284TTVBTTAzaDFpbmZON3ovY0UrclFsY0Q3d2dVZnNhckp1QkZ4SkQ5aW1zUlR4dE1qRlIrNnBPMFgyZ0l3QnFnMzhtWGVjUE1heHM1OThjTzVEMktZMG5nWDlKMkw1VWlhS3JudzVIQUpwdENURXJlMWFpOXJjbmxwMU5PYmxNZy9jVVVRaWt1aTRtRTNNQnFsaUVTT2tVM1FjNkFxQ21PNDkwSC9JQVg4UE92NG5XQkw2U3NQMjdnVjhsYkY5S3pnSkFUWmR5S1BZdzROQ0U3U3V0MXdNSEptemYyd0JxdEtLSFB6cThCZEJjRHhCTHM2WjQ5bjhFY0JYdzRnUnRxenBYQUhzbmFuc3NzZkRReG9uYVYzcEY5NEdPQUtqSnppRGR3ait2eHM2L0RWNEd2RHhSMnd1QXN4TzFMU1ZuQWFBbU96TmgyMytYc0cxVjY1TUoyejRqWWR0U1VrVVBmM1I0QzZDWjdnRzJJczM3ZHhCd1lZSjJsYytld05VSjJoMUpUQWpjSWtIYlNxL29QdEFSQURYVjJhUXIzajZTcUYzbDh6ZUoybDBDL0RCUjIxSlNGZ0JxcW5NVHRUc05PQ1JSMjhybktOSXQ1dlN6Uk8xS1NWa0FxSW1lQkM1UDFQWnhGRDRzMkZJamdIY2thdnZYcEYySlVrckNBa0JOOUV0Z2ZvSjJ4d0RISkdoWDlmQU8waXpxTko4b0FxUkdzUUJRRTUyWHFOMURnVTBUdGEzOE5pWGR3a0NwcmtrcEdRc0FOVkdxYjF0SEoycFg5ZkhHUk8xZW1xaGRLUm52ZGZvWVlOUE1BVFlrWmw4UDBuamdRV0RDZ050VnZUd0diRUlzNGpOSUk0RkhnUFVHM0s3U0tyb1BkQVJBVFhNNWcrLzhBZmJIenI4RTZ3SDdKbWgzQ2ZEYkJPMUt5VmdBcUdtdVROVHVZWW5hVmYyazJ1QXAxWk1wVWhJV0FHcWEzeVZxMXdLZ0hLbmU2MnNUdFNzbFVmVDlqdzduQURUTFpzUXVnSU8wQlRCN3dHMnEzallIN2g5d201T0lKYXJWSEVYM2dZNEFxRWtlWXZDZFA4QStDZHBVdmIwc1FadXppWW1BVWlOWUFLaEpya3ZVYm9yT1FQVzJkNkoyYjB6VXJqUndGZ0Jxa3RzU3RmdmlSTzJxdnZaTTFHNnFhMVFhT0FzQU5jbk1CRzJPQkhaTzBLN3FiU2ZTM1ArZGthQk5LUWtMQURYSnpBUnRiZzJzbmFCZDFkdTZ3T1FFN2M1TTBLYVVoQVdBbW1SbWdqYTNUOUNtbW1HSEJHMDZBcURHc0FCUWs2UjR4R3BxZ2piVkRGTVN0SGwzZ2phbEpDd0ExQlJMU2ZPSTFWWUoybFF6cENqK0hzYTFSZFFRRmdCcWlzZUFSUW5hVFhFZldNMlFvdmhiQ0R5Um9GMXA0Q3dBMUJTcEZsalpPRkc3cXIrTkVyWHJZa0JxQkFzQU5jV2ppZHFkbUtoZDFWK3E5OTRDUUkxZ0FhQ21lRHBSdXhza2FsZjFsNm9BZUNaUnU5SkFXUUNvS1JZa2FuZk5STzJxL2xLOTkvTVR0U3NObEFXQW1pTFZoK3JZUk8ycS9zWWxhdGNDUUkxZ0FhQ21TRFVDWUFGUUxnc0FGYzBDUUpLa0Fsa0FxQ2xTZlZOUE5iS2cra3YxVFQzVnlJSTBVQllBYW9wVUg2b1dBT1d5QUZEUkxBRFVGS2xHQUo1TTFLN3FMOVY3YndHZ1JyQUFVRk9za2FqZFZBc01xZjVTTGRnelBsRzcwa0JaQUtncFVpM2E4bkNpZGxWL3FkNzdEUk8xS3cyVUJZQ2F3Z0pBZzVacUJNRGxwZFVJRmdCcWluV0JNUW5hZGYvMmNzMU0wT1pZWUowRTdVb0Rad0dncGhoQm1uWDdaeVpvVTgwd00wR2JEdityTVN3QTFDUmJKbWh6Um9JMjFRd3AzdnNVMTZpVWhBV0FtbVJLZ2padlR0Q21tdUdtQkcxT1RkQ21sSVFGZ0pva3hZZnJER0J1Z25aVmI0OERzeE8wYXdHZ3hyQUFVSk5NU2REbVV1REdCTzJxM200ZzN2dEJzd0JRWTFnQXFFbTJTOVR1MVluYVZYMWRsYWpkNlluYWxRYk9Ba0JOc211aWRpOVAxSzdxSzlWN3ZuT2lkcVdCc3dCUWswd0VOay9RcmdWQWVhNU0wT1prWVAwRTdVcEpXQUNvYVhaSjBPWjl3TzBKMmxVOTNRUThrS0Jkdi8yclVTd0ExRFF2U3RUdUx4SzFxL3BKOVY2bnVqYWxKQ3dBMURSN0oyclhBcUFjNXlWcU45VzFLU1V4SW5jQ05aRGlVU0NsOHhneEYyREpnTnNkQnp4STdEbWc5bm9NMkFSWU1PQjJSeEpiUzN2OU5FdlJmYUFqQUdxYTlZRHRFN1E3SC9oSmduWlZMejlpOEowL3hOd1VPMzgxaWdXQW1taS9STzJlbWFoZDFjY1ppZHJkTjFHN1VqSVdBR3FpUXhLMWV6NXdmNksybGQ4RHdNV0oyajQwVWJ0U01oWUFhcUlEaVh2Mmc3WUlPRDFCdTZxSDd3QUxFN1E3bm5TalVsSXlGZ0Jxb3JXQWZSSzEvYTg0TWJTTmxnTC9scWp0L1lBMUU3VXRKV01Cb0tZNk1sRzdkK0lqZ1cxMExuQlhvcmFQU05TdWxGVFJqMEIwK0cydm1lNEhKakg0eHdFQkRnQitsYUJkNWJNdjhKc0U3WTRFN2lITkV0VktyK2crMEJFQU5kVm1wRnQ0NVNMZzJrUnRxM3BYa2Fiemh4ait0L05YSTFrQXFNbU9UdGoyWnhPMnJXcWxmQy9mbUxCdEthbWloejg2dkFYUVhBOENXNUptWmpmRU44YzlFN1d0YWx3TnZJUTAvODdIQXZjQ0d5Wm9XOVVvdWc5MEJFQk50Z25wSmdNQy9GM0N0bFdOVDVDdXlIODFkdjVxTUFzQU5kM3hDZHMrSC9oNXd2YVYxaG5FZkk1VWprdll0cFJjMGNNZkhkNENhTFlsd0F1QW1ZbmEzNGJZUDM1c292YVZ4aFBFbmhIM0ptcC9LdkhJcUYraW1xM29QdENMVjAwM0VqZ2hZZnQzQWljbmJGOXBmSnAwblQvRU5lZm5weHF0Nk9xbnd4R0E1cHNEVEFibUpXcC9EZUI2WWpSQTlYY1RzQnZwSm9ldUE5eE43RXlwWml1NkQ3U0NWUnVzRDd3allmdFBFOS80TEJicmJ3bndsNlRyL0NIbW5kajVxL0VzQU5RV0h3SkdKV3ovSXVEVWhPMXJNRTRpM2FJL0FLT0JEeVJzWDZxTUJZRGFZaXJ3MXNUSE9CRzRMdkV4MUwvZkV2ZitVem9HMkNyeE1hUktGSDMvbzhOaDNmYVlDVXdIRmlROHhvN0VBa0ZySlR5R2V2Y3djZDkvZHNKampBRnVCYlpPZUF4VnErZyswQkVBdGNrVTRodGFTamNCYjhQQ3NVNldBdThrYmVjUDhkeS9uYjlhbytqcXA4TVA4bmE1bXhnRmVDYnhjYjRDZkNUeE1kU2RMd0Ivay9nWWF3QzNFenRRcWoySzdnTWRBVkRiVEFZK1hNRnhQazZzTktlOGZnQjhzb0xqZkF3N2Y3Vk0wZFZQaHlNQTdUT1BHQVc0TC9GeHhnRG5BSWNrUG81VzdpTGdsY0Q4eE1mWkFyZ041MzIwVWRGOW9DTUFhcU8xZ1grbzREZ0xnVGNBMTFSd0xEM2I3NENqU04vNUEzd0pPMysxVU5IVlQ0Y2pBTzIwQk5nWHVMeUNZMjBJWEVhTU9paTlQd0o3RTl0QnA3WWZjREYrVnJaVjBlOXIwUysrd3dLZ3ZXNERYa2o2Q1lFUVR5QmNBRXlyNEZnbHV3UDRNMkJXQmNjYUIveWUyRlJJN1ZSMEgrZ3RBTFhaZE9Ddkt6cldUT0NseEdJMFN1TjN3RDVVMC9rRC9CMTIvbXF4b3F1ZkRrY0EybTBCc0Fkd1EwWEhXeHM0R3ljR0R0cEZ3R3VBdVJVZGIxZmdhbUtpcDlxcjZEN1FFUUMxM1ZqZys4UnozRldZQjd5S2VEeE5nL0VqNEhDcTYvekhBYWRqNTYrV3N3QlFDWFlFUGwvaDhSWVEreEo4Q1VlWWhtTXBzY2pQRzZobUhzZVFMd083VkhnOFNaa3NOWXFJSmNRejQxVTdDSGlnajN4TGo0Zkk4MzRkUWx3cnVWKy9VVTBVcmVqN0h4M0ZYd1FGK1JPd08rblhqRi9SSk9JMnhENFZIN2VwL2hkNEl6R3hza3BiRW1zNmJGVHhjWlZQMFgyZ3R3QlVrbzJKQ1hyaktqN3ViT0FWd0dlSWI1ZGF1YVhBcVVTaE5MUGlZNDhEZm9pZHYxU1UzRU5RUnZYeFRmTFpEN2h4RlhtVkhOY0RMeC9HZVIydWI2OGlMNlBkb2NMbHZnQ05QSEU4K1l3R1Brak1hczk5SG5MSGs4Q25pYWMxY25rMytjK0RrU2RVdU53WG9KRW5GZ0ZIa05mbXhPTm11YzlGcmppSDJMMHhwME9KUFIxeW53c2pUNmh3dVM5QUkxL01KUlo4eWUxQTRGTHluNCtxNGtyZ2dJR2N1ZUhaQ1hpTS9PZkR5QmNxWE80TDBNZ2I5d0JiVVEvN0F1ZVIvNXlraXF2STgyamZ5a3dsSm1mbVBpZEczbERoY2wrQVJ2NjRreGlPcjR0ZGlWc0RpOGgvYm9ZYlM0QUxnU01IZW9hR1p3dGlSOEhjNThiSUgwVXIraG5JanVJdkFnRndPL0VOdklvdFpydTFKZkFXNEQza3YxZmVxL3VKSXVaZmlNNjJMallFZmczc2tEc1IxVUxSZldEUkw3N0RBa0JEL2dBY1RLeENWeWVqaVJVRjN3aThHbGd2Ynpxck5BZjRIK0FNNEZmRUNFYWRiRVNNUnRSaDNvZnFvZWcrc09nWDMyRUJvT1hkU25TMjkrWk9aQlhHRW1zSkhOcUozTjlrYnlMbUxaeEhUR1Jja0RlZFZkcVU2UHgzeXAySWFxWG9QckRvRjk5aEFhQVZ6U1NLZ0RvTlhhL0twc0RlbmRpVDZPRFdUWFNzeDRsdGxhOEdMZ011cDE2M1RGWmxLK0NYd0RhNUUxSHRGTjBIRnYzaU95d0F0REt6aVMxb3I4K2RTQittQU5zVE05Mm5FUE1ITmdZbWRtSk40cmJDT3AwLy93UXhYUDhVOEVnbkhpU2VrSmpSaVZ1QVdSWGxQMGd2Qk00bEp2NUpLeXE2RHl6NnhYZFlBR2hWNWdGdkFuNldPeEgxNVdEZ0xHQkM3a1JVVzBYM2dXNEdKSzNhMnNTa3RuZmxUa1E5ZXpkUnVObjVTNnRnQVNDdDNtamd0RTdrWEs5ZTNSa0huQUo4aTNqdkpLMUMwY01mSGQ0Q1VMZXVBVjVITSsrRmwyQVNNZVMvVis1RTFCaEY5NEdPQUVqZDI1MVl6dmJnM0lub09RNERyc1hPWCtxYUJZRFVtMDJJWjk1UEkyYlRLNi94d0VuRVRQK05NdWNpTlVyUnd4OGQzZ0pRdjY0SC9weDRObDdWMndYNEwxemNSLzBydWc5MEJFRHEzeTdFdklDVGlNbG5xc1lZNEVSaVFTSTdmNmxQUlZjL0hZNEFhQkJ1Qlk0blZzaFRPdnNTR3d4dG16c1J0VUxSZmFBakFOSmdiRWVzaFg4bXNmU3NCbXR6WW5mQlM3RHpsd2FpNk9xbnd4RUFEZHBUd0plQkx3SlBaODZsNmRZQVBnRDhMY3VXTHBZR3BlZytzT2dYMzJFQm9GUm1BMThCL2htWW56bVhwaGtEdkJuNE5MR25nWlJDMFgxZzBTKyt3d0pBcWQwRmZKYVlzYjRvY3k1MU54bzRCdmgveEVaR1VrcEY5NEZGdi9nT0N3QlZaU1l4R25BYThGamVWR3BuYmVDZHdJZHhEb1dxVTNRZldQU0w3N0FBVU5YbXNHeC9nWmw1VThsdUtuQkNKOWJMbkl2S1UzUWZXUFNMNzdBQVVDNUxnSXVBYndNL0FSYmtUYWN5WTRGRGdMY0Jyd1ZHNVUxSEJTdTZEeXo2eFhkWUFLZ09IaUkyc2ptRFdFdGdTZDUwQm00azhRei9HNEhYQXh2bVRVY0NDdThEaTM3eEhSWUFxcHY3Z0xPSi9ld3ZCWjdKbTA3ZnhnUDdBVWNRdXlodWxqY2Q2VG1LN2dPTGZ2RWRGZ0NxczZlQWk0a05pSDROM0VSOVJ3ZEdBVHNDK3dPSEVwMi9HeWFwem9ydUE0dCs4UjBXQUdxU3g0RXJPbkVOc1JIUjdFeTViQW5zVEd5VC9MSk9UTWlVaTlTUG92dkFvbDk4aHdXQW1tNE9jQjF3T3pDakV6T0J1NEZINkg5eTRWaGdJakNabUswL3BmTnpPckVSMHZyRHlGbXFnNkw3d0tKZmZJY0ZnTnJ1Q2VCaDRGRmdIckFRV0F6TTdmei9DY1R3L1JqaWVmeUpuWERwWGJWZDBYMWcwUysrd3dKQWtzcFVkQi9vYm9DU0pCWElBa0NTcEFKWkFFaVNWQ0FMQUVtU0NsUjZBVkQwQkJCSktselJmV0NwTDM0Y3NlZjQ5YmtUa1NSbGN3ZndRV0N0M0lua1VObzM0STJCdndBK0FHeWVPUmRKVWozTUJiNExmSlZZUUtzSXBSUUEyd0x2Qlk0SDFzaWNpeVNwbmhZRHZ3RCtBYmd5Y3k3SnRiMEEyQWM0RVRpYzlyOVdTZExnWEFPY0N2dzNzQ2h6TGttMHNWTWNBYndHK0JTeFhya2tTZjI2RGZnYzhIMWloS0ExMmxRQWpDRDJIZjhVc1R1WkpFbURNZ000Q2ZnM1dqSWkwSllDNENEaWpiSGpseVNsMUpwQ29Pa0Z3RUhBRjRBOWNpY2lTU3BLNHd1QnBoWUFCd0JmQmw2VU94RkpVdEZ1QlQ0Qi9DUjNJcjFxV2dFd0RmZzg4SWJjaVVpU3RKeUxnWThBdjgrZFNMZWFVZ0NzVHp6Tzl5RmlGVDlKa3VwbUNmQmZSSDkxZitaY250ZW8zQWs4ajlIQWNjQ1BnWU03djVja3FZNUdBTHNDN3dZbUFMOEZGbVROYURYcVBBSndLSEF5TUQxM0lwSWs5ZUZ1NEdQQW1ia1RXWms2RmdEckV6TXIzNVU3RVVtU0J1RG53RjlTczMwRzZuWUw0QTNFT3N6NzVrNUVrcVFCbVFhY1FLd2srRnRnYWQ1MFFsMUdBTFlBL2drNEtuY2lraVFsZENXeE1kMU51UlBKUFFJd2l0aUwrY2ZBenBsemtTUXB0UzJKeWUxamdDdkl1TDlBemhHQVRZRFRpZG45a2lTVjVscmd6Y0R0T1E2ZWF3VGdLT0I4L05ZdlNTclhac1Jvd0R6Z3Fxb1BYdlVJd0hqZ2k4RDdNeHhia3FTNk9wdDQrbTFPVlFlc3NoUGVqdGhQK1lVVkhsT1NwS2FZQmJ3VnVMeUtnMVYxQytETnBKNThYQUFBRi9aSlJFRlV3TStJeVErU0pPbTUxZ1BlQmp3S1hKMzZZS2tMZ0JIRW1zamZCTVltUHBZa1NVMDNDbmdsc0RreFYyNUpxZ09sdkFXd0ZqSEwvN1VKanlGSlVsdjlCbmdkOEZDS3hsTVZBRnNRZXlQdm5xaDlTWkpLOEVmZ1ZjRE5nMjU0NUtBYkJQWUVyc0hPWDVLazRYb0JNU253Z0VFM1BPZ0M0T1hBaGNRaVA1SWthZmpXSXpZVWV2VWdHeDNrSk1CWEFPY0M2d3l3VFVtU0JLT0Ixd016Z09zSDBlQ2dDb0FqaUh2K2F3Nm9QVW1TOUd3amlaVjBad08vSDI1amd5Z0EzZ3ljZ1kvNVNaS1Uya2pnU09MSmdOOE5wNkhoRmdCSEVKMy82R0cySTBtU3VqT0NXQ3RnRnZDSDRUVFNyNWNBdnlLZTk1Y2tTZFZhU0R3aWVGNC9mN25mQW1BSFlvR0NEZnI4KzVJa2FmaWVBZzRFZnR2clgreW5BTmdjdUFMWXFvKy9LMG1TQnV0aFlHL2c5bDcrVXE4RndCcEU1KytPZnBJazFjZHR3SXVCSjdyOUM3MHVCUFJQMlBsTGtsUTMwNEYvN2VVdjlQSVV3SEhBMy9XVWppUkpxc3FPd0J6Z3FtNytjTGUzQUhZaEpoaXMwV2RTa2lRcHZZWEV5cnlYUDk4ZjdLWUFXSXQ0em5DYllTWWxTWkxTbTBWOGNaKzd1ai9VelMyQUx3S0hEeUlqU1pLVTNIckFCR0lEb1ZWNnZoR0FGd05YTXRoTmd5UkpVbHBMZ1AyQXkxYjFCMVpYQUl3aDFobmVaY0JKTmRGUzRtUmFDRWxTdlMwbW5uQWJ6a3EzYlhFTHNCc3dmMlgvYzNXUEFYNEVPLytsd0UrSmtaQzNFMFdBSkttZWxnREhFSStyLzRBb0JrcTJQZkNKWHYvU1JPQnhvZ01zTlM0a092N2x2YWNHZVJtR1lSZ3Jqdy94YkZPQlU0Q25hNUJicm5nQzJJU1ZXTldROXQ4VGp4R1VaaW53TStDdHhPVEgrMWI0LzFkMy9reUo1MGFTNnV5VHdKZFgrRytQRVJ2bG5FNDgwYllydlMrQTEzUmppUjE3eisvbUQwOGlOaGZJWGJWVUdZdUJzK24rbHNkbmFwQ3pZUmlHRWNQKzNTNVN0eTN3ZmVJelAzZmVWY1l6d09SdVR0QS8xeURaS3VNM3dJdTZPVEVyZUFld29BYjVHNFpobEJvTGdHUHAzWXVBUzJ1UWY1WHg3UlZQd29xekpOY0haZ05yZG5zV0creGU0RytBL3lST1RqOE9CSDRJckR1b3BDUkpYWmtISEEzOFloaHRIQW1jQ2t3WlJFSTFOeC9ZRW5obzZEK3NlQy9rSGJTLzgzK2FtT093TFhGZnFOL09IK0JYd0Q3QWpRUElTNUxVblJ1QXZSaGU1dzl3RHJGKy9tZUp2cUhOeHJHYTBaSVJ4SGFDdVljcFVzWTV4S3pRUVJzUG5FUjU5NVVNd3pDcWpDWEFhYVQ1b2pxSitGSzRwQWF2TTFYTVpCV1Qvdyt1UVhLcFlqWnc2TXBlOUlBZDNEbFc3dGRyR0liUnRyZ0hPSWowRHFQZG4rUC8xeGN1Znd2Z2pjTTRZWFYyRmpHNy83d0tqblVCc1Nmeko0aG5MeVZKdy9NVThWajJqc0F2S3pqZUx6ckhlczZrdVpaNHc0ci9ZUlR3Si9KWEpvT01CNEhYRHU4OERjdm14RkRWSXZLZkM4TXdqS2JGWXVCTVlDdnllU1d4SGt6dWN6SEllSmhZRitELzdGdURwQVlaUHdRMm9oNjJCLzZKMkpZeDkza3hETU9vZTh3RnZrRjhkdGJCeHNDUHlYOWVCaG43THY4Q3YxcURoQVlSaTRDUFU4OU5JQ1lBN3dOdUp2OTVNZ3pEcUZ2Y0JMd1hXSWY2R1VIYzJtM0xpTzZYaDE0VXdGWEFub001VDluTUJkNUdiTjVUZDFzVGsxbU9KQ1lPanMyYmppUlZiakh3VytMcHJGOEMxK1JOcHl1SEVDc0pycDg3a1dHNkVualpDT0xad01jN1A1dnFadURWd0IyNUUrbkRlc0Rld0U3QXpwMmYyMk5SSUtrOUZoQ2YwemNSei9EZkFGeE85RDFOc3cxeFMyQ24zSWtNdzN4Z3dnaGlNWVVyTXljekhKY1RFelhtNWs1a2dFWURHeEFyREU0Z3FzMTFLVzhUQzBuTnM0VG8yT2QwZnM0RkhpV0d6OXRpSGVCY1ZyaVgzakI3UWJPM3VMMkkyT0ZKa3FRcXJVbnNzSmU3SCt3My9uSWtjVCs2aWM0RERnZWV6SjJJSktrNFR3R3ZBbjZTTzVFK1RSMUptcVZ4VTdzV2VCM3RYN3Raa2xSZjg0bUZkUzdLblVnZnBrRE12TXc5Rk5GTDNFc3NzaU5KVWgxTUJHNG5mLy9ZUzF3Tk1Lc0dpWFFiaTRDWGR2bUdTSkpVbGUySTJ3SzUrOGx1WThaSW1qV0o3cHMwKzRrRlNWSTczUXA4T25jU1BWaHJCRkd4ckpFN2t5NDhRd3o5ejhtZGlDUkpLekVHdUJ2WU5IY2lYWGhxSk0xWmNPWmE3UHdsU2ZXMUVMZy9keEpkR2pjU21KYzdpeTd0UkROR0tpUkpaZG9MMkMxM0VsMmFONUxtcktBM0FUZ3VkeEtTSkszRVdPQmJ1WlBvd1JNUWF6UG5ubzNZYlR3R2JKSGlURWlTTkF4ZkluOGYyVXZjMUtRUkFJajE4RThuMXNxWEpLa09qZ1kra2p1SkhqMDJFcGlkTzRzZUhVQThEamppK2Y2Z0pFbUp2Ukw0VDVxM1dkc2ZSeEpiTkRiTjhjQzNhZDRKbHlTMXgzN0EyVFRuYWJybDNkclVBZ0JpUXVCLzRlMEFTVkwxWGczOG5PWStuWFlid003a240d3duRGdMR0QvZ0V5TkowcXA4RUZoTS92NXZPTEVEd0RoaWxiM2N5UXducnFXNTJ4cExrcHBoSlBCMTh2ZDV3NDJIV0c0ZTNhOXFrTkJ3NDNIZ3RhdDQweVJKR282TmdmUEkzOWNOSXM1Yy9vVjl2QVlKRFNLV0FDY0JvNTd6MWttUzFKOERnUHZJMzhjTkt2NXkrUmUzYXcwU0dtUmNDa3hEa3FUK2pRYitnZWJmNzE4eHRvVmw5d0JHRU5WTkUzWXc2dGJUeE5hTVh3TVc1VTFGQlZzWE9KaFlJM3dUWXM3TkE4UU0zSjhCTS9LbEptazFkZ0MrUS96YmJaUGZBeTlhOFQrZVN2NnFKRVZjRGV3eW5MTWw5V0VMNEorQitheisrcndFMkR0UGlwSldZaXp3S1pvL09YNVY4VmNyZTlGdHV3MndmQ3dBUGt0OCs1SlNldzJ4MFVZdjErakp1S2FGbE50ZXdJM2s3N05TeFNKZ3MxVzkrS3Rya0dES3VJdFlzOWxsaEpYS1I0bkpxUDFjbitjRGExYWZzbFM4ZFlCVGFOKzkvaFhqRjZzN0NlK3VRWUpWeFA4Qys2enVSRWg5K0NqRHZ6WXZCZGF1T25HcFVDT0JZMmpYRFAvVnhVR3JPeGtUZ0RrMVNMS0tXQUo4SDVpeXVoTWlkZWtURE83YXZBaEhBcVRVRGdGdUlIOWZWRlZjMDgxSitWUU5FcTB5bmliV0RsaS9tNU1qcmNRZ08vK2hzQWlRMHRpUkdBclAzZmRVSFVkM2MzTFdCUjZ0UWJKVngrUEE1NEVOdXpsSlVrZUt6bjhvTEFLa3daa0VuRVpNaE12ZDMxUWR0OUxEQW5uL3J3WUo1NHA1eEdTUXpiczlXU3JXSU83NVAxODRKMEFhbnNuRVovclQ1TzlmY3NYaHZaeXdDY1JtQWJtVHpobFBFWTltYmRITGlWTXhVbjd6WHpFY0NaQjZ0elh3THp6L1doeHRqL1A3T1hsL1VZUEU2eERQQU4vRTNRYTFUSldkLzFCWUJFamRtUWI4TzdDUS9QMUg3bGhJWjl2ZlhvMGdWaW5ML1FMcUVvdUJueENQVWJpT1FMbHlkUDVEWVJFZ3Jkb2V3UGNvOHg3L3F1SnJ3em1oMjlMZTVSQ0hFN2NESDhSN3M2V3A0cDcvODRWekFxUmxSZ0ZIQWhlUy85OW0zZUlXWUkzK1QyMzRiQTFlU0YxakR2QlY0QVY5bjEwMVJjNXYvaXVHSXdFcTNjYkFKNEY3eWYvdnNZNnhFSGhKMzJkM09XT0psZk55djZBNngyTGdwOFRDRWlQN084MnFzVHAxL2tOaEVhQVM3UUg4QjQ1TVAxLzhmYjhuZUdXbVVzNEtnY09OZTRqOW82ZjNkYVpWTjNYcy9JZkNJa0FsbUFDOEU3aUMvUC9tbWhCWEFHUDZPdE9yOGJvYXZMQ214ZStJdVFJdUx0Uk1kYmpuLzN6aG5BQzEwVWhpdjViVDZIMW56WkxqQVJJK3V2Nk5HcnpBSnNZendEbkFHM0RMMTZab1F1Yy9GQllCYW92SndJbkV6cTI1LzEwMUxSWUErL1oreXJzM0RoOE5IRzdNSnZZZTJLVzNVNjhLL1JYNXI1TmV3OXNCYXFvSndISEFaZlMvbGJZQjcrMzF4UGRqZmVEbURDK3VqWEU3OEFWaVlvdnE0VlUwZDA5d2l3QTF4UVRnemNEWnhLcXJ1Zi90TkQxTzd1MzBEODlVNGw1RDdoZmRwcGhCUEZMNE1ueVNJSmNOZ2NmSWZ5ME1KeXdDVkZjYkFNY1N0ME9keFQrNCtCNForb3pkaVkxemNyLzROc2FmZ05PSkJTNEdQcHRUcS9SVjhyLzNnd2puQktndUpnTEhFSjErNld2eXA0Z0xpVWYxc3ppRXNuZFlxaUllSkdiQ0hnbXMxZDNib2o2TUJlYVMvLzBlVkRnU29Gd21BeDhBZm8zTDhxYU15NmpCdi9HRHNRaW9LaFlTYi9xSnhBaU1leElNenNIa2YzOEhIWTRFcUFwckVIdWtuRVE4K3V4RXZtcitiYS9Uelp0VEJZdUFQUEVnY0NZeHhMYkI4NzVMV3AyL0p2LzdtU0lzQXBUQzFzQzdpTStmTm8yY05TRXVvWWIvcGkwQzhzWWlvdm8raWFqR25UdlFtNVBKL3g2bUNvc0FEZGZheEczSTA0Q1o1TCttUzQyZk00QU5mbExaSDVjTXJrczhEUHdRK0JCeHUyRFVxdDgyQWFlUy96MUxHYzRKVUMvV0FmNE0rQXh3T2Q3THIwTjhuNHdUL3JxMUl6Q0wvQ2ZMZUhiTUkrWVBESTBRMUxhS3pPUnZ5UDhlcFE1SEFyUXFteERmOEU4aVBpZWNzVit2T0lVR1BSNitCWEFkK1UrYXNlcVlUL3hEL3dKd09MRGVTdC9KY2h4Si92ZWtpbkFrUUNPQTdZblY5NzRMM0VuKzY5SlllU3pvdkU5SnBKeEZQb0VZZ2o0bzRURTBPRXVBRzREZkVFTitWd04vekpwUnRkWUVIZ0hHNTA2a0FoY0RSeEFyc0tuOTFnQjJJeFlaMndmWUd6Y29hNExIZ2FPQkMxSWRJUFZqWktPQkx3RWZUbndjcGZFb1VRZ3NIL2RuelNpdGZ5VzJIUzJCUlVBN2pTWDJHZGxqdWRnUk55RnJtaHVKSFhodlQzbVFxcDRqZnhQeDRlcENOczEzTDNBTjhJZmxZa2JXakFabk1uQWJaWXdDZ0VWQTA2MURkUGE3ZHVKRm5kL1hmcUtZVnVzSHhMRC9rNmtQVk9WQ01qc0JQd0ttVlhoTVZlTXhubDBRM0FEY1Fqd1cyalRIQTkvT25VU0ZMQUxxYndRd2hmZ01IZXJzZHlPZXhYY3hzUFpZQ0h5TW1QQlhpYW92bm5XQmZ3ZGVVL0Z4VmIwbHhNakFqY1R1a1VORndTM0VCTVE2T3huNFlPNGtLbVFSVUE4amlZM1dkbGd1ZGdTMnc5SFR0cHNCL0Rsd1JaVUh6VlU5SGc5OEhTL3FFaTBpTHZiYk9uRTdjRWZuMS9kbHpHdDVJNEJ2QXUvT25VaUZMQUtxc3dHdzdYSXhyUlBiNGVPNUpUb2RlRCt4b21LbGNnNGZUU08yTWR3ell3NnFseWVJZ3VCMjRnbUVHY0JkbloremdjVVY1bUlSb0g2TkFEWWp2czF2dmR6UDZjVG4zc1I4cWFsRzVoQ2ZMMmZtU2lEMy9hUFJ3S2VJZGRoZHFVNnJzd0M0bTJVRndZek83Ky9wL0x5ZnVJYzJTQllCV3BsUndLYkFWc0FrWXZMbzFPVmlDdVZNSkZWL0xnVGVRWHl4eVNaM0FURGtwY1JUQWp2a1RrU050WmpZSEdrVzhZOXFkdWZYOTNmaVQ4UVREUE42Yk5jaW9DeGppRlh4dGlRNitTMkpEbjRTeXpyN3pmQ3hPdlZuRHZBUllnR21wWGxUcVU4QkFQSG95aWVJSlZuSFpjNUY3ZlVVTWRmZ0FhSmd1SThvRGg3dS9IeWs4K3VobjR1d0NHaVRxY1FxZUpzU0hmcG14TXFsV3dDYkU1MS9uVDRYMVI1bkFSOGdQbnRxb1k0WCtuYkVZMWd2ejUySVJEemkrQkJSRU94QldkLzgybElFN0VGTXNqb2M3NytyZXZjQzd3VitranVScGhoQjdEWDlLUG5YWWphTWtxUEpld2RzUU95Z2x2c2NHbVhHZk9DTHhJSk50VlRIRVlEbGJRajhQZkhZb0pNRXBUd3VwbmtqQWRzUWVVL0tuWWlLOUhOaUNmeWtTL2tPVjkwTGdDRzdFT3NHSEpBN0VhbFFUU29DdGlRMnROb3lkeUlxenAxRXgzOXU3a1M2MFpUOWhhOEhEZ1JlU3p3R0pxbGFyeUErMU9wK08yQUU4QjNzL0ZXdFI0Qy9JcFpyYmtUbjMxVGppQlA5RVBudjhSaEdhVkgzT1FGdkp2ODVNc3FKSjRIUEU4dmNOMDVUYmdHc3pGckErNGhIQjlmTG5JdFVrdDhBcjZUM05SV3E4RnZnSmJtVFVPc3RBZjZMNkgvcXNvUjV6NXBjQUF5WlNMd0o3OFYxdEtXcVhFejk1Z1JzVFN3aExhV3ltSGl5NUhQRS9pV04xcFE1QUt2ekNMR0Y0amJBUHdIUDVFMUhLc0lyZ0I4U0srZlZoZnVLS0pWRnhPcDkyd052b3dXZFA3U2pBQmh5SDNGTFlDcndaV0pqR1VucEhFb3M0VjBYMDNNbm9OWlpRRnpqMDRHL0lIWXViWTAyRlFCREhnQStUbXpJOFNsaWhFQlNHc2NRaTNiVlFSdHVhYW9lNWdFbkU3czNIazlMbno1cll3RXc1RkhnczBRaDhGRXk3N29rdGRqUUIyVnVnOTROVXVXNW45aWRka3ZpZWY2Nzg2YVRWcHNMZ0NIemdLOFNXM2UrQ3ZobDNuU2sxbGtEK0ZydUpJak5uYVIrM0FGOGlKaEllaEt4QjBqcmxWQUFERmtDbkFQOEdmR1kwSC9qTndacFVJNEE5cytjdzcyWmo2OW1XVXhzMEhNSWNZLy9GQXFiUkY3NlBiUE5nZmNROTNnMnpweUwxSFRuQVlkbFBQNnV3Qjh5SGwvTjhCQXhzZTgwWUZibVhMSXF2UUFZTWhaNE5WRUlISWpuUmVySFVtSXVRSzVuOFRja1B0eWxsZmt0OGFqNFdjUk9mY1d6bzN1dVNjQmJpWVdGWEU5YzZzMG5pYVZSY3hoQkxFdzBQdFB4VlQrUEFXY0MzOExSb2Vld0FGaTFNY0JSd0hIQVFiZ2RzZFNOcTRDOU1oNy9MbUl0RUpWck1YQUI4Ry9BVDRsbitiVVNGZ0RkMlp6WVpPUVlZbXRpU1N1M0VOaUFmUHNFWEFic25lbll5dXMyNEh2RWluMCs5dDBGQzREZTdRQWNEYnlkV0dOQTByTzlETGd5MDdIUElQNTlxZ3ozQVdjVDkvVXZKK2FocUVzbFBRWTRLRGNEbnliMkhqaVlxRGJuWk14SHFwdHRNeDY3c1R1enFXdVBFOFA3QndHVGdROFNJejkyL2owYW5UdUJCbHNNWE5pSnNjVFRBMjhnNWcxc2tERXZLYmZOTXg3YkFxQ2RIaVBXY2ZraGNENkZQYStmaWdYQVlDd0FmdEdKVWNCTGlXTGdUYmkrZ01xemRzWmp1eGhRZTh3QnppV0c5eS9BUi9jR3pnSmc4QllUdzFHWEVYc1E3QThjMllrcDJiS1NxcFB6aVJsSEFKcHROdEhwL3hDNGhOaUdWNGxZQUtTMWtHVzNDVDVBckROOUpERTY4RktjZzZGMnlya1Z0eU1BelhNek1ieC9MazdrcTVSUEFlU3pLYkYrK3VIRS9JRjE4cVlqRGN3SndMY3pIWHN0OGoyQ3FPNDhTV3pLZG00bkhzaWJUcmtzQU9waEZQQkNZbGJya1RnNm9HWTdBTGc0NC9IbkFPdGxQTDZlNnk2aXN6K0h1RDNxSkw0YXNBQ29wNDJJWXVBUTRsSER6ZkttSTNWdEtiQUplZGZrdjRsWXIwUDVQRWpjK2p5Lzg5T3RtbXZJQXFEK1JoQWZadnQzWWoraVFKRHE2R1pneDh3NVhFQnMrNjNxUEFyOGhoajV1UVM0SHUvbDE1NlRBT3R2S2ZHTjVpWmlKNnVoZ3VBVlJERmdRYUE2T1Q5M0F2Z2tRQldHT3Z4TFdOYmhMOG1Zai9wZ0FkQTh5eGNFM3lBS2doMkJseE5Mc0w2TWVOcEF5dUVIdVJQQUp3RlN1SnVZb1g4RmNRL2ZEcjhGdkFYUVRwc1NFd24zN3Z6Y0hSaVhOU09WNERwZ04vSVAvYjZYS0k3Vm4wWEE3NG5PZmlqY1hLZUZIQUZvcHdlQUgzY0NvdlBmbmRpbWRZL09yNmRoQWFqQitoTDVPMyt3cytyVlRPQi9nYXM3OFR2aVVUMjFuQjFBdWRZQmRpV0tnYUhZSHE4SjllY200bEhXT3F6Y3RqWHd4OXhKMU5SalJBZC9PWEFOY0JYd3A2d1pLUnMvN0xXOGlVUWg4RUpnRjZKQW1BNk15Wm1VYW04cDhZVEtwWm56R0RLQ2VBeHhZdTVFTWxwS0ZFRi82TVIxblorT2p1ai9XQURvK1l3bEpobnVzbHpzaWs4ZWFKbVRnUS9uVG1JRi93aThMM2NTRlprSDNNS3pPL3JyeWJza3N4ckFBa0Q5MnBSNEhIRTdva0RZanJpRjRLSkZaYmtLMkpmWUViTk9waE8zSlhKdVREUm84NEJiZ1J1SjlSWnU2dnljUlQzbVhxaGhMQUEwYU91eHJDaVlUaFFGMDRDcHhHaUMydU5PNGttVHV0NURQZ2s0TVhjU2ZaZ04zQTdjMGZsNUsvRU5meVoyOUJvZ0N3QlZaUlF3R2RobWhaaEdUTnJ5TWNWbXVadTQ3ejhqY3g2ck00NVltZTZsdVJOWmlRZUljM2NuY0J2TE92czdjQWErS21JQm9Eb1lDV3dKVEFHMjZ2eWMzUG4xVnAxZld5RFV4OTNFU3BSMzVVNmtDeE9BbndIN1ZIemN1VVFIdjdLNEMzaTY0bnlrNTdBQVVCT01JT1ljREJVRVd3SmJBSnQzWWd0aTdzSDRYQWtXcEVtZC81Q3h3TjhDZjgxZ25taDVoaGltbjAyY2ozdVcrLzJzenM4NUF6aU9sSlFGZ05wa1E2SVFtRVFVREZzU1R5dHNBbXpjK2ZWR25UL250ZCs3Sm5iK3k1c0t2QnQ0TTNGdHJNeWZpT0g1ZXpvL1p4TjdDOXhITERGOEgrNXNwNWJ3UTFBbEdzV3lRbUJqb2xqWWtIaHVmUDFPYkxEY3I0ZCtYL0o2Q0Uzdi9GZTBNZkVVeTJoaU9QNWU0SDVnZnM2a0pFbjF0RGJ4emZGcnhHenNVbUlXYmpBbFNTcmNzY0JpOG5mS1ZjWGR3QXNHY2VJa1NXcXFZN0h6bHlTcEtNZGk1eTlKVWxHT3hjNWZrcVNpSEl1ZHZ5UkpSVGtXTzM5SmtvcHlMSGIra2lRVjVWanMvQ1ZKS3NxeDJQbExrbFNVWTdIemx5U3BLTWRpNXk5SlVsR094YzVma3FTaUhFMVpuYjhiKzBpU2lyY2I4Q1Q1TzJXLytVdVNWSkZSd0kzazc1VHQvQ1ZKcXRBeDVPK1Vxd3FIL1NWSjZ2ZzErVHRtdi9sTGtsU2g5WUNGNU8rYy9lWXZLWnVSdVJPUU10Z09HSjA3aWNUdUFRNEE3c3FkaUtSNnNnQlFpVGJPblVCaWR3UDdBMy9Nbklla0dyTUFVSWtXNWs0Z0liLzVTK3FLQllCSzlFRHVCQkx4bTc4a1Nhc3hEcGhML2tsNnp2YVhKS2xpUHlaL3ArMXNmMG1TS3JZditUdHV2L2xMa3BUQmVlVHZ3UDNtTDBsU3hUWWx2a0huN3NqOTVpOUpVc1YyQng0amY0ZnVOMzlKa2lxMk8vQW8rVHQydi9sTGtsU3hKaFFCZHY2U0pDVlE1eUxBemwrU3BJVHFXQVRZK1V1U1ZJRTZGUUYyL3BJa1ZhZ09SWUNkdnlSSkdlUXNBdXo4SlVuS0tFY1JZT2N2U1ZJTlZGa0UyUGxMa2xRalZSUUJkdjZTSk5WUXlpTEF6bCtTcEJwTFVRVFkrVXVTMUFDRExBTHMvQ1ZKYXBCQkZBRjIvcElrTmRCZXdFUDAxL25mZ1Z2NlNwTFVXRk9CUDlCYjUzOGVzSDZPWkNWSjB1Q01BVTRBWnJINmp2OEc0TFhBaUR4cFN0SXlmaEJKZ3pPQ21CdHdBREFKMklDNFJUQUR1QUM0TlY5cWtpUkpraVJKa2lSSmtpUkpraVJKa2lSSmtpUkpraVJKa2lSSmtpUkpraVJKa2lSSmtpUkpraVJKa2lSSmtpUkpraVJKa2lSSmtpUkpraVJKa2lSSlVrdjlmeDlIdnF0N2Z1MUxBQUFBQUVsRlRrU3VRbUNDXCIiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfamltdV9jb3JlX187IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHRpZDogbW9kdWxlSWQsXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSAobW9kdWxlKSA9PiB7XG5cdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuXHRcdCgpID0+IChtb2R1bGVbJ2RlZmF1bHQnXSkgOlxuXHRcdCgpID0+IChtb2R1bGUpO1xuXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCB7IGE6IGdldHRlciB9KTtcblx0cmV0dXJuIGdldHRlcjtcbn07IiwiLy8gZGVmaW5lIGdldHRlciBmdW5jdGlvbnMgZm9yIGhhcm1vbnkgZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5kID0gKGV4cG9ydHMsIGRlZmluaXRpb24pID0+IHtcblx0Zm9yKHZhciBrZXkgaW4gZGVmaW5pdGlvbikge1xuXHRcdGlmKF9fd2VicGFja19yZXF1aXJlX18ubyhkZWZpbml0aW9uLCBrZXkpICYmICFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywga2V5KSkge1xuXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIGtleSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGRlZmluaXRpb25ba2V5XSB9KTtcblx0XHR9XG5cdH1cbn07IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5vID0gKG9iaiwgcHJvcCkgPT4gKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIHByb3ApKSIsIi8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uciA9IChleHBvcnRzKSA9PiB7XG5cdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuXHR9XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5uYyA9IHVuZGVmaW5lZDsiLCIvKipcclxuICogV2VicGFjayB3aWxsIHJlcGxhY2UgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gd2l0aCBfX3dlYnBhY2tfcmVxdWlyZV9fLnAgdG8gc2V0IHRoZSBwdWJsaWMgcGF0aCBkeW5hbWljYWxseS5cclxuICogVGhlIHJlYXNvbiB3aHkgd2UgY2FuJ3Qgc2V0IHRoZSBwdWJsaWNQYXRoIGluIHdlYnBhY2sgY29uZmlnIGlzOiB3ZSBjaGFuZ2UgdGhlIHB1YmxpY1BhdGggd2hlbiBkb3dubG9hZC5cclxuICogKi9cclxuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXHJcbi8vIEB0cy1pZ25vcmVcclxuX193ZWJwYWNrX3B1YmxpY19wYXRoX18gPSB3aW5kb3cuamltdUNvbmZpZy5iYXNlVXJsXHJcbiIsImltcG9ydCB7IFJlYWN0LCBBbGxXaWRnZXRQcm9wcywgYXBwQWN0aW9ucywgSW1tdXRhYmxlIH0gZnJvbSAnamltdS1jb3JlJ1xyXG5pbXBvcnQgeyBJTUNvbmZpZyB9IGZyb20gJy4uL2NvbmZpZydcclxuXHJcbmltcG9ydCBDYW1lcmFJY29uIGZyb20gJy4vY2FtZXJhLnBuZydcclxuaW1wb3J0ICcuL3dpZGdldC5zY3NzJ1xyXG5cclxuY29uc3QgV2lkZ2V0ID0gKHByb3BzOiBBbGxXaWRnZXRQcm9wczxhbnk+KSA9PiB7XHJcbiAgY29uc3Qgc2hvd0NhbWVyYVZpZXcgPSAoKSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhwcm9wcy5kYXRhLmF0dHJpYnV0ZXMpIC8vZ2VvbWV0cnlcclxuXHJcbiAgICBjb25zdCBzciA9IEltbXV0YWJsZS5hc011dGFibGUocHJvcHMuZGF0YS5nZW9tZXRyeS5zcGF0aWFsUmVmZXJlbmNlKVxyXG5cclxuICAgIHByb3BzLndpZGdldFByb3BzLmRpc3BhdGNoKFxyXG4gICAgICBhcHBBY3Rpb25zLndpZGdldFN0YXRlUHJvcENoYW5nZShcclxuICAgICAgICAgJ3dpZGdldF8yMzcnLCAvLyd3aWRnZXRfMjM1JywgLy8nd2lkZ2V0XzIzMicsIC8vaWQgb2YgY2FtZXJhLXZpZXcgd2lkZ2V0IC4uIHNob3VsZCBiZSBjaGFuZ2VkIGlmIHRoZSB3aWRnZXQgcmVtb3ZlZCBhbmQgcmUtYWRkZWQgdXNpbmcgZXhwIGJ1aWxkZXJcclxuICAgICAgICAnc2VsZWN0ZWRDYW1lcmFWaWV3R3JhcGhpYycsXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgYXR0cmlidXRlczogcHJvcHMuZGF0YS5hdHRyaWJ1dGVzLFxyXG4gICAgICAgICAgZ2VvbWV0cnk6IHtcclxuICAgICAgICAgICAgeDogcHJvcHMuZGF0YS5nZW9tZXRyeS54LFxyXG4gICAgICAgICAgICB5OiBwcm9wcy5kYXRhLmdlb21ldHJ5LnksXHJcbiAgICAgICAgICAgIHo6IHByb3BzLmRhdGEuZ2VvbWV0cnkueixcclxuICAgICAgICAgICAgc3BhdGlhbFJlZmVyZW5jZTogc3JcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgKVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxidXR0b24gY2xhc3NOYW1lPSdjYW1lcmEtdmlldy1yZW5kZXJlcl9idG4nIG9uQ2xpY2s9e3Nob3dDYW1lcmFWaWV3fT48c3Bhbj5cclxuICAgICAgPGltZyBjbGFzc05hbWU9J2NhbWVyYS12aWV3LXJlbmRlcmVyX2ljb24nIHNyYz17Q2FtZXJhSWNvbn0gdGl0bGU9XCJDYW1lcmEgdmlld1wiIC8+XHJcbiAgICAgIDwvc3Bhbj48L2J1dHRvbj5cclxuICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFdpZGdldFxyXG4iXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=