System.register([], function(__WEBPACK_DYNAMIC_EXPORT__, __system_context__) {


	return {

		execute: function() {
			__WEBPACK_DYNAMIC_EXPORT__(
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
/*!******************************************************************!*\
  !*** ./your-extensions/widgets/redux/src/extensions/my-store.ts ***!
  \******************************************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MyActionKeys": () => (/* binding */ MyActionKeys),
/* harmony export */   "default": () => (/* binding */ MyReduxStoreExtension)
/* harmony export */ });
var MyActionKeys;
(function (MyActionKeys) {
    MyActionKeys["MyAction1"] = "MY_ACTION_1";
    MyActionKeys["MyAction2"] = "MY_ACTION_2";
})(MyActionKeys || (MyActionKeys = {}));
class MyReduxStoreExtension {
    constructor() {
        this.id = 'my-local-redux-store-extension';
    }
    getActions() {
        return Object.keys(MyActionKeys).map(k => MyActionKeys[k]);
    }
    getInitLocalState() {
        return {
            a: null,
            b: null
        };
    }
    getReducer() {
        return (localState, action, appState) => {
            switch (action.type) {
                case MyActionKeys.MyAction1:
                    return localState.set('a', action.val);
                case MyActionKeys.MyAction2:
                    return localState.set('b', action.val);
                default:
                    return localState;
            }
        };
    }
    getStoreKey() {
        return 'myState';
    }
}

/******/ 	return __webpack_exports__;
/******/ })()

			);
		}
	};
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0cy9yZWR1eC9kaXN0L2V4dGVuc2lvbnMvbXktc3RvcmUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O1VBQUE7VUFDQTs7Ozs7V0NEQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLHlDQUF5Qyx3Q0FBd0M7V0FDakY7V0FDQTtXQUNBOzs7OztXQ1BBOzs7OztXQ0FBO1dBQ0E7V0FDQTtXQUNBLHVEQUF1RCxpQkFBaUI7V0FDeEU7V0FDQSxnREFBZ0QsYUFBYTtXQUM3RDs7Ozs7Ozs7Ozs7OztBQ0pBLElBQVksWUFHWDtBQUhELFdBQVksWUFBWTtJQUN0Qix5Q0FBeUI7SUFDekIseUNBQXlCO0FBQzNCLENBQUMsRUFIVyxZQUFZLEtBQVosWUFBWSxRQUd2QjtBQTBCYyxNQUFNLHFCQUFxQjtJQUExQztRQUNFLE9BQUUsR0FBRyxnQ0FBZ0M7SUE2QnZDLENBQUM7SUEzQkMsVUFBVTtRQUNSLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELGlCQUFpQjtRQUNmLE9BQU87WUFDTCxDQUFDLEVBQUUsSUFBSTtZQUNQLENBQUMsRUFBRSxJQUFJO1NBQ1I7SUFDSCxDQUFDO0lBRUQsVUFBVTtRQUNSLE9BQU8sQ0FBQyxVQUFxQixFQUFFLE1BQW1CLEVBQUUsUUFBaUIsRUFBYSxFQUFFO1lBQ2xGLFFBQVEsTUFBTSxDQUFDLElBQUksRUFBRTtnQkFDbkIsS0FBSyxZQUFZLENBQUMsU0FBUztvQkFDekIsT0FBTyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUN4QyxLQUFLLFlBQVksQ0FBQyxTQUFTO29CQUN6QixPQUFPLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0JBQ3hDO29CQUNFLE9BQU8sVUFBVTthQUNwQjtRQUNILENBQUM7SUFDSCxDQUFDO0lBRUQsV0FBVztRQUNULE9BQU8sU0FBUztJQUNsQixDQUFDO0NBQ0YiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9leGItY2xpZW50L3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL2V4Yi1jbGllbnQvd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovL2V4Yi1jbGllbnQvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly9leGItY2xpZW50L3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC8uL3lvdXItZXh0ZW5zaW9ucy93aWRnZXRzL3JlZHV4L3NyYy9leHRlbnNpb25zL215LXN0b3JlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIFRoZSByZXF1aXJlIHNjb3BlXG52YXIgX193ZWJwYWNrX3JlcXVpcmVfXyA9IHt9O1xuXG4iLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gKGV4cG9ydHMpID0+IHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiaW1wb3J0IHsgdHlwZSBleHRlbnNpb25TcGVjLCB0eXBlIEltbXV0YWJsZU9iamVjdCwgdHlwZSBJTVN0YXRlIH0gZnJvbSAnamltdS1jb3JlJ1xyXG5cclxuZXhwb3J0IGVudW0gTXlBY3Rpb25LZXlzIHtcclxuICBNeUFjdGlvbjEgPSAnTVlfQUNUSU9OXzEnLFxyXG4gIE15QWN0aW9uMiA9ICdNWV9BQ1RJT05fMicsXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQWN0aW9uMSB7XHJcbiAgdHlwZTogTXlBY3Rpb25LZXlzLk15QWN0aW9uMVxyXG4gIHZhbDogc3RyaW5nXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQWN0aW9uMiB7XHJcbiAgdHlwZTogTXlBY3Rpb25LZXlzLk15QWN0aW9uMlxyXG4gIHZhbDogc3RyaW5nXHJcbn1cclxuXHJcbnR5cGUgQWN0aW9uVHlwZXMgPSBBY3Rpb24xIHwgQWN0aW9uMlxyXG5cclxuaW50ZXJmYWNlIE15U3RhdGUge1xyXG4gIGE6IHN0cmluZ1xyXG4gIGI6IHN0cmluZ1xyXG59XHJcbnR5cGUgSU1NeVN0YXRlID0gSW1tdXRhYmxlT2JqZWN0PE15U3RhdGU+XHJcblxyXG5kZWNsYXJlIG1vZHVsZSAnamltdS1jb3JlL2xpYi90eXBlcy9zdGF0ZSd7XHJcbiAgaW50ZXJmYWNlIFN0YXRlIHtcclxuICAgIG15U3RhdGU/OiBJTU15U3RhdGVcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE15UmVkdXhTdG9yZUV4dGVuc2lvbiBpbXBsZW1lbnRzIGV4dGVuc2lvblNwZWMuUmVkdXhTdG9yZUV4dGVuc2lvbiB7XHJcbiAgaWQgPSAnbXktbG9jYWwtcmVkdXgtc3RvcmUtZXh0ZW5zaW9uJ1xyXG5cclxuICBnZXRBY3Rpb25zICgpIHtcclxuICAgIHJldHVybiBPYmplY3Qua2V5cyhNeUFjdGlvbktleXMpLm1hcChrID0+IE15QWN0aW9uS2V5c1trXSlcclxuICB9XHJcblxyXG4gIGdldEluaXRMb2NhbFN0YXRlICgpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGE6IG51bGwsXHJcbiAgICAgIGI6IG51bGxcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldFJlZHVjZXIgKCkge1xyXG4gICAgcmV0dXJuIChsb2NhbFN0YXRlOiBJTU15U3RhdGUsIGFjdGlvbjogQWN0aW9uVHlwZXMsIGFwcFN0YXRlOiBJTVN0YXRlKTogSU1NeVN0YXRlID0+IHtcclxuICAgICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIGNhc2UgTXlBY3Rpb25LZXlzLk15QWN0aW9uMTpcclxuICAgICAgICAgIHJldHVybiBsb2NhbFN0YXRlLnNldCgnYScsIGFjdGlvbi52YWwpXHJcbiAgICAgICAgY2FzZSBNeUFjdGlvbktleXMuTXlBY3Rpb24yOlxyXG4gICAgICAgICAgcmV0dXJuIGxvY2FsU3RhdGUuc2V0KCdiJywgYWN0aW9uLnZhbClcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgcmV0dXJuIGxvY2FsU3RhdGVcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0U3RvcmVLZXkgKCkge1xyXG4gICAgcmV0dXJuICdteVN0YXRlJ1xyXG4gIH1cclxufVxyXG4iXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=